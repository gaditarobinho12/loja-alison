<section id="store-content">
    		<div class="container">
    		    <div class="inner-content">
        			<div class="row">
    <div class="col-xs-12">
		<h2 class="page-title" style="color: #de058e;">Identificação</h2>
		<hr>
	</div>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" >Já sou cadastrado</h3>
            </div>
            <div class="panel-body">
                <form method="POST" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <input type="text" name="email" class="form-control" placeholder="Entre com seu e-mail" autocomplete="off" />
                    </div>
                    <div class="input-group margin-top">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input type="password" name="senha" class="form-control" placeholder="Sua senha" autocomplete="off" />
                    </div>
                    <div class="clearfix">
                        <label class="checkbox pull-right">
                            <input name="remember" value="true" type="checkbox" />
                            Continuar conectado
                        </label>
                        <!--<span class="margin-top10 pull-left">
                            <a href="forgot_password.html">
                                Esqueceu a senha?
                            </a>
                        </span>-->
                    </div>
                    <input type="submit" value="Login" name="submit" class="btn btn-primary margin-top pull-right" style="background-color: #de058e;"/>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Criar uma nova conta</h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo BASE_URL;?>register" method="post" accept-charset="utf-8"><div style="display:none">
<input type="hidden" name="only_email" value="1" />
<input type="hidden" name="redirect" value="secure/my_account/" />
<input type="hidden" name="new" value="new" />
<input type="hidden" name="submitted" value="submitted" />
</div>
                    <p>Digite o e-mail que deseja cadastrar</p>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-plus"></i>
                        </span>
                        <input type="text" name="email1" class="form-control" placeholder="Entre com seu e-mail" autocomplete="off" />
                    </div>
                    <input type="submit" value="Cadastre-se" name="submit" class="btn btn-primary margin-top pull-right" style="background-color: #de058e;"/>
                </form>
            </div>
        </div>
    </div>
</div>

		        
    		    </div>
    		</div>
    	</section>