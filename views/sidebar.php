<div class="row" style="margin-top: -60px;" style="">
    <div class="col-xs-12" >
        <ol class="breadcrumb">
        <li><a href="<?php echo BASE_URL;?>">Página Principal</a></li>

                                                                                                                                    
        <li class="active">
            <?php if(isset($viewData['category_name'])):?>
                <?php echo $viewData['category_name'];?>
            <?php endif;?>

            <?php if(isset($_GET['s'])):?>
                <?php echo $_GET['s'];?>
            <?php endif;?>

        </li>

        </ol>     
    </div>
</div>
<div class="sidebar" style="">
<div class="row">
    <div class="col-xs-12">
    </div>
        <div class="col-xs-12">
            <div class="side-header" style="background-color: #de058e;"><i class="glyphicon glyphicon-filter"></i> Filtrar Por</div>

            <ul class="filters-side filters price-filters">

            <div class="hide"></div>

            <?php foreach($viewData['categories'] as $cat): ?>

                <li><span><a style="background-color: transparent; color: #333;">
                <?php echo $cat['name']; ?>
            </a></span></li>

            <?php
                if(count($cat['subs']) > 0) {
                    $this->loadView('menu_subcategory', array(
                        'subs' => $cat['subs'],
                        'level' => 1
                    ));
                }
            ?>
                
            <?php endforeach;?>     
        </ul>
    </div>
</div>
</div>