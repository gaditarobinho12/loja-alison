<div class="row">
<div class="col-xs-12">
<div class="checkout-box m-top30">
<div class="row cart-bottom-buffer-md paymentwrap">
<div class="col-md-12 header">
<h2 style="color: #de058e;">
Cartão de crédito
</h2>

<div id="payment_tabs">

<div class="tab-content">

<div id="payment-pagseguro_app" class="tab-pane payment-method inactive active" style="height: auto;">
<style type="text/css">
.pagseguro-form label,
.pagseguro-form span,
.pagseguro-form div,
.pagseguro-form p{
font-family: 'Arial';
}
.pagseguro-logo{
width: 100px;
}
#pagseguro_container label{
color: #666;
font-weight: 600;
}
#pagseguro_container .form-control{
font-size: 20px;
height: auto;
padding: 10px;
}
#pagseguro_container input.form-control{
padding: 10px 20px;
}
.pagseguro_cc_card_num, .pagseguro_cc_card_code, .pagseguro_cc_cpf{
text-align: center;
}
#card-flag{
position: absolute;
top: 0;
right: 15px;
}
.pagseguro-important{
color: #F37B20;
}
.pagseguro-type-header{
color: #666;
font-size: 20px;
}
.pagseguro-type-list{
color: #F37B20;
list-style: outside none disc;
line-height: 145%;
margin-left: 15px;
padding: 10px 0px;
}
.pagseguro-type-list li{
font-size: 18px;
font-weight: 300;
margin-bottom: 15px;
}
.pagseguro_radio input[type="radio"]{
display: none;
}
.pagseguro_radio img{
cursor: pointer;
opacity: 0.4;
filter: Alpha(opacity=40);
}
.pagseguro-creditcard-brands img.selected{
opacity: 1;
filter: Alpha(opacity=100);
}
.pagseguro-type-warning{
color: #666;
font-size: 16px;
}
.pagseguro-barcode{
color: #666;
font-weight: 600;
text-align: center;
}
.pagseguro-barcode .glyphicon-barcode{
font-size: 35px;
}
.pagseguro_cc_card_list{
text-align-last: center;
font-family: Courier;
}
.pagseguro-app-type-prize-in{
background: #09A51C none repeat scroll 0% 0%;
color: #FFF;
padding: 5px;
}

   /* This is a compiled file, you should be editing the file in the templates directory */
.pace {
  -webkit-pointer-events: none;
  pointer-events: none;

  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #e90f92;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}
</style>

<div id="pagseguro_container">
<div class="well">
<div class="row pagseguro-payment-options">
<div class="col-md-2 col-xs-12">
<div class="pagseguro_options">
<br>
<p><label>Escolha abaixo:</label></p>
<ul class="list-inline">
<li>
<label>
<input type="radio" class="pagseguro_radio module_payment_method" checked="">
<img src="https://mist-store.xtechcommerce.com/assets/img/f-credito.png" alt="Cartão de Crédito">
</label>
</li>
</ul>
</div>
</div> <!-- col-md-2 -->
<br>
<div class="col-md-10 col-xs-12">


<div class="pagseguro-form pagseguro-creditCard" style="display: block;">

    <!--<input type="hidden" name="email" value="<?php echo $user['email'];?>">-->
    <input type="hidden" name="email" value="c11261910667778156711@sandbox.pagseguro.com.br">

    <input type="hidden" name="name" value="<?php echo $user['name']." ".$user['nick'];?>">
    <input type="hidden" name="email" value="<?php echo $user['email'];?>">

    <input type="hidden" name="cep" value="<?php echo $user['cep'];?>">
    <input type="hidden" name="rua" value="<?php echo $user['endereco'];?>">
    <input type="hidden" name="numero" value="<?php echo $user['numero'];?>">
    <input type="hidden" name="complemento" value="<?php echo $user['complemento'];?>">
    <input type="hidden" name="cidade" value="<?php echo $user['cidade'];?>">
    <input type="hidden" name="telefone" value="<?php echo $user['telefone'];?>">
    <input type="hidden" name="bairro" value="<?php echo $user['bairro'];?>">
    <input type="hidden" name="estado" value="<?php echo $user['estado'];?>">
    <input type="hidden" name="total" value="<?php echo $total;?>" />

    <div class="form-group">
        <div class="row">
        <div class="col-md-6 col-xs-12">
        <label>Número do cartão</label>
        <input type="text" class="form-control pagseguro_cc_card_num" maxlength="16" style="font-family:Courier;" name="cartao_numero">
        </div>
        <div class="col-md-6 col-xs-12">
        <label>Expira em</label>
        <div class="row">
        <div class="col-sm-5 col-xs-12">
        <select class="form-control pagseguro_cc_exp_date_mm" style="font-family:Courier;" name="cartao_mes">
        <option value="">Mês</option>
        <?php for($q=1; $q<=12; $q++):?>
            <option><?php echo ($q < 10)?'0'.$q:$q;?></option>
        <?php endfor;?>
        </select>
        </div>
        <div class="col-sm-6 col-xs-12">
        <select class="form-control pagseguro_cc_exp_date_yy" style="font-family:Courier;" name="cartao_ano">
        <option value="0" selected="selected">Ano</option>
        <?php $ano = intval(date('Y'));?>
        <?php for($q=$ano; $q<=($ano+20); $q++):?>
            <option><?php echo $q;?></option>
        <?php endfor;?>

        </select>
        </div>
        </div>
        </div>
        </div>
        <br>
        <div class="form-group">
        <div class="row">
        <div class="col-md-8 col-xs-12">
        <label>Nome do titular <small>(Como escrito no cartão)</small></label>
        <input type="text" name="cartao_titular" class="form-control pagseguro_cc_embossing" style="font-family:Courier;" data-get="fullname">
        </div>
        <div class="col-md-4 col-xs-12">
        <label>Código de segurança</label>
        <div class="clearfix"></div>
        <input type="text" class="form-control pagseguro_cc_card_code" style="font-family:Courier; float:left; width:50%;" name="cartao_cvv">
        <img src="https://mist-store.xtechcommerce.com/assets/img/card-security-code.png" style="float: left; margin: 5px 0 0 10px;" alt="Código de segurança">
        </div>
        </div>
        </div>
        </div>

        <div class="form-group">
        <div class="row">
        <div class="col-md-6 col-xs-12">
        <label>CPF do titular</label>
        <input type="text" name="cartao_cpf" class="form-control pagseguro_cc_cpf" style="font-family:Courier;">
        </div>

        <div class="form-group">
            <div class="row">
                <div id="pagseguro-cc-installments" class="col-md-6 col-xs-12">
                <label>Número de parcelas</label>
                <select name="parc" class="form-control pagseguro-installments">
                </select>
            </div>
            </div>
        </div>
        </div>

<div class="row">
<div class="col-md-12">
<br>
<p class="pagseguro-important">
Importante:<br>
O limite disponível no cartão de crédito deve ser superior ao valor total da compra, e não ao valor de cada parcela.
</p>
</div>
</div>
</div>
</div> <!-- pagseguro-creditcard -->

</div> <!-- col-md-10 -->
</div>
</div>
</div>

</div>

</div>

</div>
</div>
</div>
</div>

<div class="row">
<div class="col-xs-12">
<div class="cart-bottom-buffer-md text-right cart-country">
Esta compra está sendo feita no
<strong>Brasil</strong>
<span class="country-flag">
<img src="https://mist-store.xtechcommerce.com/loja/themes/default/assets/img/br.png" alt="Brasil">
</span>
</div>
</div>
</div>


<div class="row cart-bottom-buffer-md text-center cart-actions">
<button class="btn btn-lg btn-primary efetuarCompra" style="background-color: #de058e;">
<i class="glyphicon glyphicon-ok"></i> Finalizar compra
</button>
</div>


</div>
</div>
<script type="text/javascript">
    BASE_URL = '<?php echo BASE_URL;?>';
</script>
<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/psckttransparente.js"></script>

<script type="text/javascript">
    PagSeguroDirectPayment.setSessionId("<?php echo $sessionCode;?>");
</script>

<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/pace.min.js"></script>

