<?php $c = new Cloud();?>
<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Editar Comentário
        </h2>
    </div>
</div>

<div class="row">
<div class="col-xs-12">
<form class="form-horizontal" method="POST" accept-charset="utf-8" >
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Atualizar comentário</h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome da marca *
        </label>
        <div class="col-xs-5">
        <textarea name="comentario" class="form-control"><?php echo $infoComents['comment'];?></textarea>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Pontos
        </label>
        <div class="col-xs-5">
        	<?php for($q=0; $q < $infoComents['points']; $q++):?>
                <img src="<?php echo BASE_URL;?>assets/images/star.png">
            <?php endfor;?>
        </div>
        </div>


        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>

    

</div>
</div>