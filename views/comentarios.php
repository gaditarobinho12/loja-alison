<?php $c = new Cloud();?>
<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Comentários
        </h2>
    </div>
</div>

<div class="row">
	<table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Produto</th>
                    <th>Estrelas</th>
                    <th>Comentario</th>
                    <th>Data</th>
                    <th>Editar/Excluir</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($comentarios as $item):?>
                	<?php $cliente = $c->getDadosCliente($item['id_user']);?>
                	<?php $produto = $c->getDadosProduct($item['id_product']);?>
                <tr>
                    <td><?php echo $item['id'];?></td>
                    <td><?php echo substr($cliente['name']." ".$cliente['nick'], 0, 25);?></td>
                    <td><a href="<?php echo BASE_URL;?>product/open/<?php echo $produto['id'];?>" target="_blank"><?php echo $produto['name'];?></a></td>
                    <td>
                    	<?php for($q=0; $q < $item['points']; $q++):?>
                    		<img src="<?php echo BASE_URL;?>assets/images/star.png">
                    	<?php endfor;?>
                    </td>
                    <td title="<?php echo $item['comment'];?>"><?php echo substr($item['comment'], 0, 28)."..."; ?></td>
                    <td><?php echo $item['date_rated'];?></td>
                    <td><a href="<?php echo BASE_URL;?>cloud/editarComentario/<?php echo $item['id'];?>" class="btn btn-default">EDITAR</a>
                    	<a href="<?php echo BASE_URL;?>cloud/excluirComentario/<?php echo $item['id'];?>" class="btn btn-danger" onclick="return confirm('Você tem certeza que quer excluir este comentario?')">EXCLUIR</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
</div>