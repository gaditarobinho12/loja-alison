<div class="row">
	<div class="col-xs-12">
		<h2 class="category-header" style="color: #de058e;">
			<?php echo $viewData['category_name'];?>
		</h2>
	</div>
</div>

<!--<div class="row">
	<div id="filter-widget" class="col-xs-12">
		<nav class="cat_options">
		<ul class="filters order">
			<li>Organizar por:</li>
			<li><a class="sub" href="#order_price">Menor Preço</a></li>
		</ul>
		</nav>
	</div>
</div>-->


<div class="">
<?php
$a = 0;
?>
<?php foreach($viewData['list'] as $product_item): ?>

		<?php $this->loadView('product_item2', $product_item); ?>

<?php endforeach; ?>

</div>

<ul class="pagination col-md-12">
 
  	<?php for($q=1;$q<=$numberOfPages;$q++): ?>

	 <li class="page-item"><a class="page-link" href="<?php echo BASE_URL; ?>?<?php
		$pag_array = $_GET;
		$pag_array['p'] = $q;
		echo http_build_query($pag_array);
	?>"><?php echo $q; ?></a></li>
	<?php endfor; ?>
	
 
</ul>
