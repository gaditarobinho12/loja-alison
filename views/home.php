<div class="">
<?php
$a = 0;
?>
<?php foreach($list as $product_item): ?>

		<?php $this->loadView('product_item', $product_item); ?>

<?php endforeach; ?>

</div>

<ul class="pagination col-md-12">
 
  	<?php for($q=1;$q<=$numberOfPages;$q++): ?>

	 <li class="page-item"><a class="page-link" href="<?php echo BASE_URL; ?>?<?php
		$pag_array = $_GET;
		$pag_array['p'] = $q;
		echo http_build_query($pag_array);
	?>"><?php echo $q; ?></a></li>
	<?php endfor; ?>
	
 
</ul>