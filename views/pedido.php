<?php $p = new Pedido();?>
<div class="col-xs-9 main">
<div class="tab-content">
    <div class="tab-pane active" id="dashboard">
    <h2 class="category-header" style="color: #de058e;">Pedido #<?php echo $viewData['idP'];?></h2>
    <div class="panel panel-default">
    <table class="table table-striped">
        <thead>
            <tr>
               	<th style="text-align: left;">Produto</th>
                <th style="text-align: left;">Tamanho</th>
                <th style="text-align:left;">Quantidade</th>
                <th style="text-align:left;">Preço (UNIDADE)</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($itensP as $item):?>
				<tr>
					<?php $nomeP = $p->getNome($item['id_product']);?>
				<td><a href="<?php echo BASE_URL; ?>product/open/<?php echo $item['id_product']; ?>"><?php echo $nomeP['name'];?></a></td>
				<td><?php echo $item['size'];?></td>
				<td><?php echo $item['quantity'];?></td>
				<td>R$<?php echo number_format($item['product_price'],2, ",", ".")?></td>
				</tr>
				
			<?php endforeach;?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>

<div class="col-xs-3 sidebar" style="margin-top: 40px;">
	<div class="side-header" style="background-color: #de058e;">
            <i class="glyphicon glyphicon-send"></i> Codigo de rastreio
        </div>
       <div id="mytabs" style="border: #ccc 1px solid; height: auto;">
       	<?php if(!empty($resumo['objeto'])):?>
       		<h3 style="margin-left: 25px;"><?php echo $resumo['objeto'];?></h3>
       	<?php else:?>
       		<div class="alert alert-danger">
       			O seu pedido ainda não foi postado, Por favor espere o tempo de verificação (2 a 3 dias úteis).
       		</div>
       	<?php endif;?>

       </div>
</div>