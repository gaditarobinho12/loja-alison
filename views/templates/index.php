<!DOCTYPE html>
<html lang="pt-BR" dir="ltr">
    <head>
        

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">


            <meta name="robots" content="index,follow">
  <script type="text/javascript" src="https://cdn.xtechcommerce.com/assets/js/lang/pt.js"></script>
<script type="text/javascript">
var site_url = {"address_form":"http:\/\/<?php echo BASE_URL;?>\/secure\/address_form","delete_address":"http:\/\/<?php echo BASE_URL;?>\/secure\/delete_address","base":"http:\/\/<?php echo BASE_URL;?>\/","add_to_cart":"http:\/\/<?php echo BASE_URL;?>\/cart\/add_to_cart","save_cart":"http:\/\/<?php echo BASE_URL;?>\/cart\/save_cart","my_orders":"http:\/\/<?php echo BASE_URL;?>\/secure\/my_orders","set_address":"http:\/\/<?php echo BASE_URL;?>\/cart\/set_address","busca_cep":"http:\/\/<?php echo BASE_URL;?>\/shipping\/busca_cep","get_frete":"http:\/\/<?php echo BASE_URL;?>\/shipping\/get_frete","search_list":"http:\/\/<?php echo BASE_URL;?>\/search\/listing","search_ajax":"http:\/\/<?php echo BASE_URL;?>\/search\/find","variant_detail":"http:\/\/<?php echo BASE_URL;?>\/cart\/variant"};
</script>
		<!-- BEGIN - Google Analytics -->
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-81663408-1', 'auto', {'allowLinker': true});
ga('require', 'displayfeatures');
ga('require', 'linker');
ga('linker:autoLink', ['mist-store.xtechcommerce.com'] );
ga('send', 'pageview');
ga('create', 'UA-52707601-3', 'auto', {'name': 'customTracker', 'allowLinker': true});
ga('customTracker.require', 'displayfeatures');
ga('customTracker.require', 'linker');
ga('customTracker.linker:autoLink', ['mist-store.xtechcommerce.com'] );
ga('customTracker.send', 'pageview');
</script>
<!-- END - Google Analytics -->





<!-- BEGIN - Pushcrew -->
<!-- END - Pushcrew -->

<!-- BEGIN - Facebook Pixel -->
<!-- END - Facebook Pixel -->

<!-- BEGIN - Criteo -->
<!-- END - Criteo -->

<!-- BEGIN - reCaptcha -->
<!-- END - reCaptcha -->

<!-- New label delivered -->
<style type="text/css">
.label-win {
  background-color: #5cb85c;
}
.label-win[href]:hover,
.label-win[href]:focus {
  background-color: #449d44;
}
</style>

        <title>New In | Página inicial  | MIST STORE</title>
        
        <link href="<?php echo BASE_URL; ?>assets/css/bootstrap.mine548.css" type="text/css" rel="stylesheet" />
    	<link href="<?php echo BASE_URL; ?>assets/css/applicatione548.css" type="text/css" rel="stylesheet" />
    	<link href="<?php echo BASE_URL; ?>assets/css/jquery.fancyboxe548.css" type="text/css" rel="stylesheet" />
    	<link href="<?php echo BASE_URL; ?>assets/css/font-awesome.mine548.css" type="text/css" rel="stylesheet" />
    	<link href="<?php echo BASE_URL; ?>assets/css/style.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.equalheights.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/imgLiquid-min.js"></script>
        
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/application.js"></script>
    
                <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
<script>WebFont.load({google: {families: ['Didact+Gothic:100,200,300,400,500,600,700,800,900']}});</script>

<script>
$(function(){
    $('a').click(function(){
        $('#carregando').show();
    });
});
</script>

<style>    
        #topcontrol .top_button{
        margin-bottom: 50px;
        color: #de058e;
        background-color: #de058e;
    }
        /* FONT */
        div,span,p,h1,h2,h3,h4,h5,h6{
            font-family: Didact Gothic;
        }
    
    /* BG */
        body{
            background-color: #ffffff; 
                background-attachment: inherit;
            }
        
        /* BOX */
        .box{
            box-shadow: 0 0 10px #bbbbbb;
        }
        
        /* TOPO */
        #topbar{
            background-color: #000000;
        }
            
        /* HEADER */
                header .logo-base{
            background-color: #ffffff;
        }
                
        /* MENU */
                #menu .navbar-nav{
            background-color: #ffffff;
        }
        
        /* CONTENT */
                #h-social .inner-content,
        #blog .inner-content,
        #store-content .inner-content{
            background-color: #ffffff;
        }
        
        /* FOOTER */
                footer .block{
            background-color: #ffffff;
        }
            
    
    /* ADJUST */
        .adjust{
            margin-top: px;
        }
        
    /* ALTURA IMAGEM */
        #store-content .product-list-item .prod-wrapper .prod-image{
            height: 453px;
        }
    
        #store-content .cat-prod-list .prod-wrapper .prod-image{
            height: 402px;
        }
        
    /* BUTTONS */
        #store-content .btn-primary{
            background: #000000;
            color: #ffffff;
        }

        #store-content .btn-primary:hover{
            background: #666666;
        }
        
        #topcontrol .top_button{
            background: #de058e;
        }
        
        #topcontrol .top_button:hover{
            background: #333333;
        }
        
        #topcontrol .top_button:after{
            border-bottom-color: #ffffff;
        }
    
    /* BORDERS */
        /* TOP */
                    #topbar .list-inline{
                border-top: none;
            }
                
                    #topbar .list-inline{
                border-bottom: none;
            }
                
        /* MENU */
                   #menu .navbar{
                border-top: none;
            }
                
                    #menu .navbar{
                border-bottom: none;
            }
                
                    #menu .navbar-nav>li:hover{
                border-left: 1px solid transparent;
                border-right: 1px solid transparent;
            }
                
        /* LIST PROD */
                    #store-content .prod-wrapper .prod-action{
                border-top: 1px solid #e4e4e4;
            }
                            #store-content .prod-wrapper .prod-action{
                border-bottom: none;
            }
                
        /* FOOTER */
                                    footer .block:first-child{
                    border-top: 2px solid #000000;
                }  
                                        footer .config{
                border-top: none;
            }  
                            footer .block:last-child{
                border-bottom: none;
            }
                
        /* LANGUAGE */
                    footer .config{
                border-top: 1px dotted #aaaaaa;
            }
                            footer .config{
                border-bottom: 1px dotted #aaaaaa;
            }
                
    /* LINKS & BUTTONS */
    a{
        color: #000000;
    }
    
    a:hover{
        color: #666666;
    }
    
    a:focus{
        color: #666666;
        text-decoration: none;
    }
    
    #topbar .list-inline li a{
        color: #ffffff;
    }    
    
    #topbar .list-inline li a:hover{
        color: #969696;
    }
    
    footer .block button,
    #store-content .btn-primary,
    #blog .blog-item .readmore{
        background: #000000;
        color: #ffffff;
    }
    
    footer .block button:hover,
    #store-content .btn-primary:hober,
    #blog .blog-item .readmore:hover{
        background: #666666;
    }
    
    #topcontrol .top_button{
        background: #000000;
    }
    
    #topcontrol .top_button:hover{
        background: #333333;
    }
    
    #topcontrol .top_button:after{
        border-bottom-color: #ffffff;
    }
    
    #store-content .add-to-wishlist .btn-primary,
    #store-content .prod-size-table .btn-primary{
        background: #000000;
        color: #ffffff;
    }
    
    #store-content .add-to-wishlist .btn-primary:hover,
    #store-content .prod-size-table .btn-primary:hover{
        background: #727272;
    }
    
    /* TOP */
    
    #topbar .list-inline i.glyphicon-shopping-cart{
        background: #000000;
        color: #ffffff;
    }
    
    /* SEARCH */
    
    #top-search .input-group input :-moz-placeholder,
    #top-search .input-group input ::-webkit-input-placeholder{
        color: #000000;
    }
    
    #top-search .input-group{
        border-color: #dddddd;
    }
    
    #top-search .input-group input{
        background: #fafafa;
        color: #000000;
    }
    
    #top-search .input-group button{
        background: #fafafa;
    }
    
    #top-search .input-group button i{
        color: #666666;
        
    }
    
    #top-search .input-group button:hover i{
        color: ;
    }
    
    #top-search .input-group button:hover{
        background: #e4e4e4;
    }
    
    .arrowup{
        border-bottom-color: #ffffff;
    }
    
    #search-results-in{
        background: #ffffff;
    }
    
    #search-results a:hover, #search-results a.up{
        background: #fafafa;
    }
    
    #search-results .line .desc .name{
        color: rgba(0,0,0,1);
    }
    
    #search-results .line .desc .category{
        color: #999999;
    }
    
    #search-results .line .desc .price .saleprice{
        color: #999999;
    }
    
    #search-results .line .desc .price{
        color: #000000;
    }
    
    #search-results .last{
        background: #ffffff;
        color: #666666;
    }
    
    #search-results .last a:hover, #search-results .last a.up{
        background: #fafafa;
        color: #333333;
    }
    
    /* MENU */
    
    #menu .navbar-nav .dropdown-menu,
    #menu .navbar-nav .dropdown-submenu{
        background: #000000;
    }
    
    #menu .navbar-nav .dropdown-menu a{
        color: #eeeeee;
    }
    
    #menu .navbar-nav .dropdown-menu a .sub-caret{
        border-left-color:#eeeeee!important;
    }
     
    #menu .navbar-nav .dropdown-menu a:hover{
        color: #ffffff;
    }
    
    #menu .navbar-nav>li a .caret{
        border-top-color: #000000;;
        border-bottom-color: #000000;;
    }
    
    .navbar-default .navbar-nav>li>a{
        color: #000000;
    }
    
    #menu .navbar-nav>li a:hover .caret{
        border-top-color: #969696;;
        border-bottom-color: #969696;;
    }
    
    .navbar-default .navbar-nav>li>a:hover,
    .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover, .navbar-default .navbar-nav>.open>a:focus{
        color: #969696;
    }
    
    /* BANNER */
    .carousel-control .arrowright:after,
    .carousel-control.left{
        border-left-color: #ffffff;
    }
    .carousel-control .arrowleft:after,
    .carousel-control.right{
        border-right-color: #ffffff;
    }
    
    .carousel-control .arrowright{
        border-left-color: #666666;
    }
    
    .carousel-control.right:hover .arrowright{
        border-left-color: #333333;
    }
    
    .carousel-control .arrowleft{
        border-right-color: #666666;
    }
    
    .carousel-control.left:hover .arrowleft{
        border-right-color: #333333;
    }
    
    .carousel-indicators .active{
        background: #ffffff;
    }
    
    .carousel-indicators li{
        border-color: #ffffff;
    }
    
    /* SIDEBAR */
    
    .sidebar .side-header{
        background: #000000;
        color: #fffcfc;
    }
    
    .sidebar ul{
        border-color: #e4e4e4;
    }
    
    .sidebar ul li a{
        background: #ffffff;
        color: #666666;
    }
    
    .sidebar ul li a:hover{
        background: #e4e4e4;
        color: #000000;
    }
    
    /* PRODLIST */
    #store-content .prod-wrapper .prod-info,
    #store-content .prod-wrapper{
        background: #ffffff;
    }
    
    #store-content .prod-wrapper .prod-info a{
        color: #000000;
    }
    
    #store-content .prod-wrapper .prod-info a:hover{
        color: #707070;
    }
    
    #store-content .prod-wrapper .prod-info p .slash{
        color: #000000;
    }
    
    #store-content .prod-wrapper .prod-info .buy-price strong,
    #store-content #ProdModal .prod-price strong{
        color: #720303;
    }
    
    #store-content .prod-wrapper .prod-info .installments-price strong,
    #store-content .prod-wrapper .prod-info .installments-price{
        color: #939393;
    }
    
    #store-content .prod-wrapper .prod-action{
        background: #ffffff;
        color: ;
    }
    
    #store-content .prod-wrapper .prod-action:hover{
        background: #000000;
        border-color: #000000;
    }
    
    #store-content .prod-wrapper .prod-action a,
    #store-content .prod-wrapper .prod-action span{
        color: #000000;
    }
    
    #store-content .prod-wrapper .prod-action:hover a,
    #store-content .prod-wrapper .prod-action:hover span{
        color: #ffffff;
    }
    
    /* PRODUCT */
    
    #gallery img{
        border-color: #dddddd;
    }
    
    #store-content #product .prod-image-thumbs .active img, #gallery a:hover img{
        border-color: #bbbbbb;
    }
    
    #store-content #product .showcase .prod-image{
        border-color: #ffffff;
    }
    
    #store-content #product .showcase .prod-image img{
        border-color: #ffffff;
    }
    
    .zoomWindow{
        border-color: #000000;
    }
    
    /* GUIDELINE */
    
    .block-header span{
        color: #000000;
    }
    
    .block-header{
        border-bottom-color: #000000;
    }
    
    #store-content .prod-title{
        color: #000000;
    }
    
    #store-content .prod-sub{
        background: #ffffff;
        border-top-color: #cccccc;
        color: #919191;
    }
    
    #store-content .prod-sub .prod-ref {
        color: #919191;
    }
    
    #store-content .prod-sub .prod-secure{
        color: #ffffff;
    }
    
    #store-content .prod-excerpt p{
        color: #4f4f4f;
    }
    
    #store-content .prod-description .tab-content {
        color: ;
    }
    
    #product .prod-action .estoque {
        color: ;
    }
    
    #product .prod-action .estoque span {
        color: ;
        font-weight: bold;
    }
    
    #store-content .prod-frete,
    #store-content #product .prod-variants{
        background: #f9f9f9;
        border-color: #bbbbbb;
    }
    
    #store-content #product .showcase .prod-action{
        background: #ffffff;
    }
    
    #product .prod-action .buy-price .slash{
        color: #999999;
    }
    
    #look .buy-price .onsale,
    #product .prod-action .buy-price .sale{
        color: #000000;
    }
    
    #product .prod-action .installments-price{
        color: #999999;
    }
    
    #product .prod-action .buy-btn{
        background: #000000;
        color: #ffffff;
    }
    
    #product .prod-action .buy-btn:hover{
        background: #686868;
    }
    
    #product .buy-message{
        background: #ffffff;
        color: #000000;
    }
    
    #product .prod-action .buy-btn-form .alert-danger{
        background: #e5e5e5;
        color: #000000;
    }
    
    #product .prod-action .remindme-form fieldset label{
        color: #000000;
    }
    
     #product .prod-action .remindme-form fieldset input[type=submit]{
        background: #000000;
        color: #ffffff;
    }
    
    #product .prod-action .remindme-form fieldset input[type=submit]:hover{
        background: #878787;
    }
    
    /* PAGE */
    
    .page-header{
        border-bottom-color: ;
        color: #333333;
    }
    
    /* FLAGS */
    
    #store-content .promo{
        background: #000000;
        color: #ffffff;
    }
    
    #store-content .off{
        background: #000000 ;
        color: #ffffff ;
    }
    
    #store-content .out{
        background: #000000 ;
        color: #ffffff ;
    }
    
    /* BLOG */
    
    #blog .blog-item .time{
        color: ;
    }
    
    #blog .blog-item .title a{
        color: #000000;
    }
    
    #blog .blog-item .title a:hover{
        color: #333333;
    }
    
    #blog .blog-item .resume{
        background: #f0f0f0;
        color: #333333;
    }
    
    .blog-item .item-date,
    .blog-item .item-title{
        background: #000000;
    }
    
    .blog-item .item-date{
        color: #ffffff;
    }

    .blog-item .item-title a{
        color: #666666;
    }
    
    .blog-item .item-title a:hover{
        color: #ffffff;
    }
    
    
    
        
        .blog-item img:hover{
            transform:scale(1.1);
        }
        
        
    /* POPUP */
    
    #popup .modal-content,
    #exit_popup .modal-content{
        background: #ffffff;
    }
    
    #popup .modal-body h1,
    #exit_popup .modal-body h1{
        color: #000000;
    }
    
    #popup .modal-body p,
    #exit_popup .modal-body p{
        color: #000000;
    }
    
    #popup .modal-body span,
    #exit_popup .modal-body span{
        color: #666666;
    }
    
    #popup button,
    #exit_popup button{
        background: #000000;
        color: #ffffff;
    }
    
    #popup button:hover,
    #exit_popup button:hover{
        background: #666666;
    }
    
    #popup .close,
    #exit_popup .close{
        background: #000000;
        color: #ffffff;
    }
    
    #popup .close:hover,
    #exit_popup .close:hover{
        background: #666666;
    }
    
    /* FOOTER */
    
    footer p,
    footer small,
    footer h4,
    footer .config ul li,
    footer .links ul li{
        color: #333333;
    }
    
    footer .links ul li a{
        color: #000000;
    }
    
    footer .links ul li a:hover{
        color: #666666;
    }
    
    footer form input{
        background: #ffffff;
        border-color: #e4e4e4;
        color: #666666;
    }
    
    footer form input:focus{
        border-color: #999999;
    }
    
    footer .block button{
        background: #000000;
    }
    
    footer .block button:hover{
        background: #666666;
    }
    
    .social-icons{
        color: #666666;
    }
    
    .social-icons:hover{
        color: #000000;
    }
    
    #ProdModal .btn-finalizar{
        background-color: green;
        color: #fff;
    }
    
        
    #flash .alert{
        background-color:
    }
    
    #flash .alert .close-alert{
        color:;
    }

    #flash .alert-danger{
        color:
    }
    
    #flash .alert-warning{
        color:
    }
    
    #flash .alert-info{
        color:
    }
    
    #cartview .cart_finish{
        border-color:#000000;
        color:#ffffff;
        background-color:#000000;
    }
    
    </style>


        
                        <!--[if IE]><link rel="shortcut icon" href="https://assets.xtechcommerce.com/assets/7100/1466470959/img/favicon.ico?v=1503003150"><![endif]-->
        <link rel="icon" href="<?php echo BASE_URL;?>assets/images/fav.png">
        <link rel="apple-touch-icon" href="<?php echo BASE_URL;?>assets/images/fav.png">
        
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', 'b88a7d956a6341cf7400adebdb548323919872b5');
        </script>
                
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
                    </head>
    
    				
    <body itemscope itemtype="http://schema.org/Organization" class="pagina-inicial">

        

        
<!-- BEGIN - Facebook Pixel -->
<!-- END - Facebook Pixel -->
            	    	    	<section id="flash">
    		<div class="container-fluid">
    			<div class="row">
    				<div class="col-xs-12">
    					    					    					    				
    				                        	                    	                    	    				    
    				</div>
    			</div>
    		</div>
    	</section>
        
                
                <div id="modal-container" class="modal fade"></div>
        
                        <section id="topbar" style="background-color: #de048e;"">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-inline">
            <li id="top-search">
         
         
         
                        <form action="<?php echo BASE_URL; ?>busca" method="GET" id="form-search">
                            <div class="input-group">
                                <input type="text" name="s" id="search-engine" class="form-control" placeholder="Buscar" autocomplete="off"  value="<?php echo (!empty($viewData['searchTerm']))?$viewData['searchTerm']:''; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>	
             
         
    </li>
    <ul class="pull-right">
        <li>
            <a href="tel:WhatsApp (27)  99722-7542">
            <i class="glyphicon glyphicon-earphone"></i>    WhatsApp (27)  99722-7542
            </a>
        </li>
        <?php if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])):?>
            <li>
                <a href="<?php echo BASE_URL;?>users">
                <i class="glyphicon glyphicon-user"></i>
                    <?php echo $viewData['user']['name']; ?>
                </a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>users/sair">
                <i class="glyphicon glyphicon-log-out"></i>
                    Sair
                </a>
            </li>
        <?php else:?>
            <li>
            <a href="<?php echo BASE_URL;?>login">
                <i class="glyphicon glyphicon-log-in"></i>
                Entrar
                </a>
            </li>
        <?php endif;?>
        <li>
            <a href="<?php echo BASE_URL;?>cart">
                <i class="glyphicon glyphicon-shopping-cart" style="background-color: #de048e;"></i>
                Carrinho: <span id="view_cartTotalValue"><?php echo "R$ ".number_format($viewData['cart_subtotal'], 2, ',', '.'); ?></span>
            </a>
        </li>
    </ul>
    </ul>                    </div>
                </div>
            </div>
        </section>
        
        
                        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="logo-base">
                        <a href="<?php echo BASE_URL;?>">
                            <img class="logo" src="<?php echo BASE_URL;?>assets/images/logo.jpg" width="350px" alt="Toda Chique">
                        </a>            
                        </h1>
                    </div>
                                    </div>
            </div>
        </header>
        
        
        
                
        
        
        
        
        <section id="menu" style="margin-bottom: 30px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <nav class="navbar navbar-default" role="navigation">
                            <ul class="nav navbar-nav" style="background-color: transparent;">
                                <?php foreach($viewData['categoriesMenu'] as $cat): ?>
                                    <li>
                                        <a href="<?php echo BASE_URL.'categories/enter/'.$cat['id']; ?>">
                                        <h2 style="color: #de048e;"><?php echo $cat['name'];?></h2>
                                        </a>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </nav>

                    </div>
                </div>   
            </div>             
        </section>
        
        
        
        
          <?php if(isset($viewData['slider'])):?>
          
          
                    		<section id="main-banners">
		        <div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
															<div class="item active">
									<a href="" class="slide"><img src="https://assets.xtechcommerce.com/uploads/banners/cd6b4d32acbe26df9892b32853af8b35.jpg" alt="Banner" /></a>
								</div>
															<div class="item">
									<a href="" class="slide"><img src="https://assets.xtechcommerce.com/uploads/banners/e721460e85235d2e5bb13c6ba11f715e.jpg" alt="Banner" /></a>
								</div>
													</div>
													<ol class="carousel-indicators">
							  								<li data-target="#carousel" data-slide-to="0" class="active"></li>
							  								<li data-target="#carousel" data-slide-to="1" class="passive"></li>
							  							</ol>
																		</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(window).load(function(){
				$('.carousel-control').css('opacity','1');
				var ch = ($('.carousel-inner').height()-60)/2;
				$('.carousel-control.left').css("top",ch);
				$('.carousel-control.right').css("top",ch);
			});
		</script>
						<div class="container">
    		<div class="row">
    		                        <div class="col-xs-4">
                        <div class="bannerExtra">
    		                  <img src="<?php echo BASE_URL;?>assets/images/subslide1.jpg" />
    		            </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="bannerExtra">
    		                    		                    <img src="<?php echo BASE_URL;?>assets/images/subslide2.jpg" />
    		                    		            </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="bannerExtra">
    		                    		                    <img src="<?php echo BASE_URL;?>assets/images/subslide3.jpg" />
    		                    		            </div>
                    </div>
    		        		</div>
		</div>
			</section>
			
		
          <?php endif;?>
          
        
        	
    			<section id="store-content">
    		<div class="container">
    		    <div class="inner-content">
    		    	
    		    	
							
				 <?php if(isset($viewData['slider'])): ?>			
		    			 <h2 class="block-header" style="border-color: #de048e;"><span style="color: #de048e;">Destaques</span></h2>					
		    	 <?php endif; ?>
                  	 
	<div class="row prod-list">
        <?php if(isset($viewData['sidebar'])): ?>
            <div class="col-md-3">
                <?php $this->loadView('sidebar', array('viewData'=>$viewData)); ?>
            </div>
            <div class="col-md-9"><?php $this->loadViewInTemplate($viewName, $viewData); ?></div>
        <?php else: ?>
            <div class="col-sm-12-"><?php $this->loadViewInTemplate($viewName, $viewData); ?></div>
        <?php endif; ?>
    </div>
	
		        
    		    </div>
    		</div>
    	</section>
    	    	    	
    			<footer>
    		<div class="container">
    			<div class="row">
    				<div class="col-xs-12">
    					        					<div class="block links" style="border-color: #de048e;">
        						<div class="row">
        							<div class="col-xs-3">
        								<h4 style="color: #de048e;">Institucional</h4>
        								<ul>      									        						<li>
        										<a href="<?php echo BASE_URL;?>contato" >Contato</a>
        									</li>
        									        								</ul>
        							</div>
        							<div class="col-xs-3">
        								<h4 style="color: #de048e;">Ajuda & Suporte</h4>
        								<ul>
        								            									        									<li>
        										<a href="<?php echo BASE_URL;?>estatics/perguntasFrequentes" >Política de Entrega</a>
        									</li>
        									        									<li>
        										<a href="<?php echo BASE_URL;?>troca" >Política de Troca</a>
        									</li>
        									        								</ul>
        							</div>
        							<div class="col-xs-6">
        							    <div class="row">
        							        <div class="col-xs-8">
                						        <h4 style="color: #de048e;">Newsletter</h4>
                								<form action="https://todachiqueloja.us12.list-manage.com/subscribe/post?u=c5d790978a8fe31625b5a6a0c&amp;id=dde0f686c5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

                                <div class="input-group">
                                    <input type="text" name="EMAIL" id="mce-EMAIL" placeholder="Insira seu e-mail" class="form-control required email" />
                                    <span class="input-group-btn">
                                    <button type="submit" class="button btn btn-sub" style="background-color: #de048e;">
                                    Assinar 
                                    </button>
                                    </span>
                                </div>
                            </form>
                							</div>
<div class="col-xs-4">
<h4 style="color: #de048e;">Redes Sociais</h4>
<div class="social">
 <a class="social-icons" href="http://www.facebook.com.br/todachiqueserra" target="_blank" style="color: #de058e;">
<i class="fa fa-lg fa-fw fa-facebook"></i>
</a>
<a class="social-icons" href="https://www.instagram.com/todachiqueloja/" target="_blank" style="color: #de058e;">
<i class="fa fa-lg fa-fw fa-instagram"></i>
</a>
            									                								</div>
</div>  
        							    </div>
        							            							</div>
        						</div>
        					</div>
        					
    					    					<div class="block config" style="border-color: #de048e;">
    					    <div class="row">
    					        <div class="col-xs-12 text-center payments">
								    <p><img src="<?php echo BASE_URL;?>/assets/images/f-visa.png"></p>
                                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-mastercard.png"></p>
                                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-diners.png"></p>
                                    <p><img src="<?php echo BASE_URL;?>assets/images/f-elo.png"></p>
                                    <p><img src="<?php echo BASE_URL;?>assets/images/f-boleto.png"></p>
                                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-cielo.png"></p>    								    							        							        							        					        </div>
    					            					    </div>
    					</div>
    <div class="block">
        <div class="row">
        <div class="col-xs-12 text-center">                         
        <p><small>
        <strong>CNPJ:</strong> 13.601.852/0001-86<br>
        <strong>Razão Social:</strong> TODA CHIQUE COMERCIOS E CALÇADOS.<br>
        </small></p>
        <p><small>&copy; 2018 TODA CHIQUE. Todos os direitos reservados.</small></p>
        </div>
        <div class="col-xs-12 text-center">
        <p><small>
        &nbsp;&nbsp;&nbsp;
        <span style="color: transparent;">Powered by</span>
        <a href="<?php echo URL_ALISON;?>" target="_blank">&nbsp;
        <span style="color: transparent;">Alison Vitor Bucker</span>
        </a>
        </small></p>
        </div>
        </div>  
        </div>
        </div>
    				</div>
    			</div>
    		</div>
    	</footer>
        
                        
        
            
        
    
<!-- BEGIN - Instaby -->
<!-- END - Instaby -->

<!-- BEGIN - Cartstack -->
	<script src="https://app.cartstack.com/br/activeAPI/load.js" type="text/javascript"></script>
<!-- END - Cartstack -->

<!-- BEGIN - SMARTHINT -->
<!-- END - SMARTHINT -->

<!-- BEGIN - ROI HERO -->
<!-- END - ROI HERO -->

<!-- BEGIN - Enviou -->
	<script> var CLIENT_TOKEN='03082017051510ZTT'; </script> <script src="https://ca.enviou.com.br/js/ca-xtech.js"> </script>
<!-- END - Enviou -->

<!-- BEGIN - Moxchat -->
	<script type="text/javascript">
!function(t){var e=t.createElement("script");e.type="text/javascript",e.charset="utf-8",
e.src="https://static.moxchat.it/visitor-widget-loader/wN2Dmopn1P.js",e.async=!0;
var a=t.getElementsByTagName("script")[0];a.parentNode.insertBefore(e,a);
}(document);
</script>
<!-- END - Moxchat -->

<!-- BEGIN - Hariken -->
	<script type='text/javascript'>var hkn = { code: '8e6ce7d6-83fd-4968-98b3-a2fd6fc64828' };(function(){var h = document.createElement('script'); h.type = 'text/javascript'; h.async = true;h.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tag.hariken.co/hkn.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(h, s);})();</script>
<!-- END - Hariken -->

<!-- BEGIN - Konduto -->
<!-- END - Konduto -->

<!-- BEGIN - Shopback -->
<!-- END - Shopback -->

<!-- BEGIN - JivoChat -->
<!-- END - JivoChat -->


<!-- BEGIN - E-bit -->
<!-- END - E-bit -->

<!-- BEGIN - Clearsale -->
<!-- END - Clearsale -->

<!-- BEGIN - Trustvox -->
<!-- END - Trustvox -->
<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/pace.min.js"></script>

<style type="text/css">
/* This is a compiled file, you should be editing the file in the templates directory */
.pace {
  -webkit-pointer-events: none;
  pointer-events: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #e90f92;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}

.pace .pace-progress-inner {
  display: block;
  position: absolute;
  right: 0px;
  width: 100px;
  height: 100%;
  box-shadow: 0 0 10px #e90f92, 0 0 5px #e90f92;
  opacity: 1.0;
  -webkit-transform: rotate(3deg) translate(0px, -4px);
  -moz-transform: rotate(3deg) translate(0px, -4px);
  -ms-transform: rotate(3deg) translate(0px, -4px);
  -o-transform: rotate(3deg) translate(0px, -4px);
  transform: rotate(3deg) translate(0px, -4px);
}

.pace .pace-activity {
  display: block;
  position: fixed;
  z-index: 2000;
  top: 15px;
  right: 15px;
  width: 14px;
  height: 14px;
  border: solid 2px transparent;
  border-top-color: #e90f92;
  border-left-color: #e90f92;
  border-radius: 10px;
  -webkit-animation: pace-spinner 400ms linear infinite;
  -moz-animation: pace-spinner 400ms linear infinite;
  -ms-animation: pace-spinner 400ms linear infinite;
  -o-animation: pace-spinner 400ms linear infinite;
  animation: pace-spinner 400ms linear infinite;
}

@-webkit-keyframes pace-spinner {
  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
}
@-moz-keyframes pace-spinner {
  0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
}
@-o-keyframes pace-spinner {
  0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
}
@-ms-keyframes pace-spinner {
  0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
}
@keyframes pace-spinner {
  0% { transform: rotate(0deg); transform: rotate(0deg); }
  100% { transform: rotate(360deg); transform: rotate(360deg); }
}


</style>
    </body>
</html>