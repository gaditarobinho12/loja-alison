<html lang="pt-BR" dir="ltr">
    <head>
        

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index,follow">
    <script type="text/javascript" src="<?php echo BASE_URL; ?>/assets/js/lang/pt.js"></script>
    <title>Toda chique | Administrativo</title>
    <link href="<?php echo BASE_URL; ?>assets/css/bootstrap.mine548.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/css/applicatione548.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/css/jquery.fancyboxe548.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/css/font-awesome.mine548.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/css/style.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.equalheights.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/imgLiquid-min.js"></script>

    <script type="text/javascript" src="<?php echo BASE_URL; ?>js/applicatione548.js"></script>

    <script src="<?php echo BASE_URL; ?>assets/js/webfont.js"></script>
    <script>WebFont.load({google: {families: ['Didact+Gothic:100,200,300,400,500,600,700,800,900']}});</script>

    <link rel="icon" href="<?php echo BASE_URL; ?>assets/images/fav.png">
    <link rel="apple-touch-icon" href="<?php echo BASE_URL; ?>assets/images/fav.png">

    <script type="text/javascript">
        window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='<?php echo BASE_URL;?>assets/js/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', 'b88a7d956a6341cf7400adebdb548323919872b5');
    </script>

    

    </head>

    <body itemscope itemtype="http://schema.org/Organization" class="pagina-inicial">

        <section id="flash">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                                                                                            
                                                                                                                        
                    </div>
                </div>
            </div>
        </section>

        <div id="modal-container" class="modal fade"></div>

        <section id="topbar">
            <div class="container" >
                <div class="row">
                    <div class="col-xs-12" >
                        <ul class="list-inline" style="margin-bottom: -13px;">
                    <li id="top-search">
         
                        <form action="<?php echo BASE_URL; ?>busca" method="GET" id="form-search">
                            <div class="input-group">
                                <input type="text" name="s" id="search-engine" class="form-control" placeholder="Buscar" autocomplete="off"  value="<?php echo (!empty($viewData['searchTerm']))?$viewData['searchTerm']:''; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </li>
                    <ul class="pull-right">
                        <li>
                            <a href="tel:WhatsApp (27)  99722-7542">
                            <i class="glyphicon glyphicon-earphone"></i>    WhatsApp (27)  99722-7542
                            </a>
                        </li>
                        <?php if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])):?>
                            <li>
                                <a href="<?php echo BASE_URL;?>cloud/sair">
                                <i class="glyphicon glyphicon-log-out"></i>
                                    Sair
                                </a>
                            </li>
                        <?php else:?>
                            <li>
                            <a href="<?php echo BASE_URL;?>login">
                                <i class="glyphicon glyphicon-log-in"></i>
                                Entrar
                                </a>
                            </li>
                        <?php endif;?>
                        
                    </ul>
            </ul>                    
            </div>
            </div>
            </div>
        </section>

         <header>
            <div class="container" style="margin-bottom: -35px;">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="logo-base">
                        <a href="<?php echo BASE_URL;?>">
                            <!--<img class="logo" src="<?php echo BASE_URL; ?>assets/images/logo.jpg" width="350px" alt="Toda Chique">-->
                            <img class="logo" src="<?php echo BASE_URL; ?>assets/images/logo.jpg" alt="Toda Chique" width="350px">
                        </a>            
                        </h1>
                    </div>
                </div>
            </div>
        </header>

        <section id="menu" style="margin-bottom: 30px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])):?>
                            <nav class="navbar navbar-default" role="navigation">
                                <ul class="nav navbar-nav">
                                    
                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud">
                                            <h2>Novos Pedidos</h2>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud/visualisados">
                                            <h2>Pedidos Atendidos</h2>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud/categorias">
                                            <h2>Categorias</h2>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud/marcas">
                                            <h2>Marcas</h2>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud/produtos">
                                            <h2>Produtos</h2>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo BASE_URL;?>cloud/comentarios">
                                            <h2>Comentarios</h2>
                                            </a>
                                        </li>
                                    
                                </ul>
                            </nav>
                        <?php endif;?>
                    </div>
                </div>   
            </div>             
        </section>

        
        <?php if(isset($viewData['slider'])):?>
            <section id="main-banners">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="carousel" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">
                                <div class="item active">
                                <a href="" class="slide"><img src="<?php echo BASE_URL;?>assets/images/s1.jpg"/></a>
                                </div>
                                
                                </div>

                                <ol class="carousel-indicators">
                                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel" data-slide-to="1" class="passive"></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                $(window).load(function(){
                $('.carousel-control').css('opacity','1');
                var ch = ($('.carousel-inner').height()-60)/2;
                $('.carousel-control.left').css("top",ch);
                $('.carousel-control.right').css("top",ch);
                });
                </script>

                <div class="container">
                <div class="row">
                    <div class="col-xs-4">
                    <div class="bannerExtra">
                    <img src="<?php echo BASE_URL;?>assets/images/subslide1.jpg" />
                    </div>
                    </div>

                    <div class="col-xs-4">
                    <div class="bannerExtra">
                    <img src="<?php echo BASE_URL;?>assets/images/subslide2.jpg" />
                    </div>
                    </div>

                    <div class="col-xs-4">
                    <div class="bannerExtra">
                    <img src="<?php echo BASE_URL;?>assets/images/subslide3.jpg" />
                    </div>
                    </div>

                </div>
                </div>
                </section>

            <section id="store-content">
             <div class="container">
            <div class="inner-content">

                <h2 class="block-header"><span>Novidades</span></h2>
        <?php else:?>
            <div class="container">
            <div class="inner-content">
        <?php endif;?>


<div class="row prod-list">
<script type="text/javascript">
function change_pic(el) {
var option = el.find('a').data('opt');
el.parents('.prod-wrapper').find('.ig-option').hide();
el.parents('.prod-wrapper').find('.ig-'+option).show();
}

$(document).ready(function(){
$('.l-color').on('click', function(){
change_pic($(this));
});
});
</script>

    <section class="">
        <div class="row prod-list">
        <?php if(isset($viewData['sidebar'])): ?>
            <div class="col-sm-3">
                <?php $this->loadView('sidebar', array('viewData'=>$viewData)); ?>
            </div>
            <div class="col-sm-9"><?php $this->loadViewInTemplate($viewName, $viewData); ?></div>
        <?php else: ?>
            <div class="col-sm-12"><?php $this->loadViewInTemplate($viewName, $viewData); ?></div>
        <?php endif; ?>
    </div>
    </section>

<footer>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="block links">
                <div class="row">
                    <div class="col-xs-3">
                        <h4>Institucional</h4>
                        <ul>
                            <li>
                                <a href="<?php echo BASE_URL;?>contato" >Contato</a>
                            </li>
                        </ul>
                    </div>

                <div class="col-xs-3">
                    <h4>Ajuda & Suporte</h4>
                    <ul>
                        <li>
                            <a href="<?php echo BASE_URL;?>estatics/perguntasFrequentes" >Política de Entrega</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL;?>troca" >Política de Troca</a>
                        </li>
                    </ul>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-8">
                            <h4>Newsletter</h4>

                            <form action="https://todachiqueloja.us12.list-manage.com/subscribe/post?u=c5d790978a8fe31625b5a6a0c&amp;id=dde0f686c5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

                                <div class="input-group">
                                    <input type="text" name="EMAIL" id="mce-EMAIL" placeholder="Insira seu e-mail" class="form-control required email" />
                                    <span class="input-group-btn">
                                    <button type="submit" class="button btn btn-sub">
                                    Assinar 
                                    </button>
                                    </span>
                                </div>
                            </form>
                        </div>

                        <div class="col-xs-4">
                            <h4>Redes Sociais</h4>
                            <div class="social">
                                <a class="social-icons" href="http://www.facebook.com.br/todachiqueserra" target="_blank">
                                <i class="fa fa-lg fa-fw fa-facebook"></i>
                                </a>
                                <a class="social-icons" href="https://www.instagram.com/todachiqueloja/" target="_blank">
                                <i class="fa fa-lg fa-fw fa-instagram"></i>
                                </a>
                            </div>
                        </div> 

                    </div>
                </div>

                </div>
            </div>

        <div class="block config">
            <div class="row">
                <div class="col-xs-12 text-center payments">
                    <p><img src="<?php echo BASE_URL;?>/assets/images/f-visa.png"></p>
                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-mastercard.png"></p>
                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-diners.png"></p>
                    <p><img src="<?php echo BASE_URL;?>assets/images/f-elo.png"></p>
                    <p><img src="<?php echo BASE_URL;?>assets/images/f-boleto.png"></p>
                    <p><img src="<?php echo BASE_URL; ?>assets/images/f-cielo.png"></p>
                </div>
            </div>
        </div>

        <div class="block">
        <div class="row">
        <div class="col-xs-12 text-center">                         
        <p><small>
        <strong>CNPJ:</strong> 13.601.852/0001-86<br>
        <strong>Razão Social:</strong> TODA CHIQUE COMERCIOS E CALÇADOS.<br>
        </small></p>
        <p><small>&copy; 2018 TODA CHIQUE. Todos os direitos reservados.</small></p>
        </div>
        <div class="col-xs-12 text-center">
        <p><small>
        &nbsp;&nbsp;&nbsp;
        <span style="color: transparent;">Powered by</span>
        <a href="<?php echo URL_ALISON;?>" target="_blank">&nbsp;
        <span style="color: transparent;">Alison Vitor Bucker</span>
        </a>
        </small></p>
        </div>
        </div>  
        </div>
        </div>
    </div>
</div>
</footer>

<div class="top_button"></div>

<script type="text/javascript">
!function(t){var e=t.createElement("script");e.type="text/javascript",e.charset="utf-8",
e.src="https://static.moxchat.it/visitor-widget-loader/JNv0exEBWl.js",e.async=!0;
var a=t.getElementsByTagName("script")[0];a.parentNode.insertBefore(e,a);
}(document);
</script>


