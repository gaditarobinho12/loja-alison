<div class="text-center">
	<h1>Obrigada!</h1>
	<p>Sua compra foi aprovada! Embreve seu pagamento será liberado. Você poderá acompanhar o status do pagamento entrando no <strong><a href="<?php echo BASE_URL;?>users">SEU PERFIL</a></strong>, e clicando em <strong>HISTÓRICO DE PEDIDOS</strong>.</p>
	<p>Lá você verá o status do pagamento, e quando seu pedido for postado, terá acesso ao codigo de rastreio, e poderá acompanhar seu pedido diretamente no site dos correios.</p>
</div>

