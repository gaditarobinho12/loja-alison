<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Categorias
        </h2>
        <span class="pull-right">
        	<a href="<?php echo BASE_URL;?>cloud/addCat" class="btn btn-success">Adicionar Nova</a>
        </span>
    </div>
</div>

<div class="row">
	<table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Filho de:</th>
                    <th>Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($cats as $item):?>

                <tr>
                    <td><?php echo $item['id'];?></td>
                    <td><?php echo $item['name'];?></td>
                    <td><?php echo $item['sub'];?></td>
                    <td><a href="<?php echo BASE_URL;?>cloud/editarCat/<?php echo $item['id'];?>" class="btn btn-default">EDITAR</a>
                    	<a href="<?php echo BASE_URL;?>cloud/excluirCat/<?php echo $item['id'];?>" class="btn btn-danger" onclick="return confirm('Você tem certeza que quer excluir esta categoria?')">EXCLUIR</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
	<?php foreach($cats as $item):?>
		
	<?php endforeach;?>
</div>