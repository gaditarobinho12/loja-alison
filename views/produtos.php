<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Produtos
        </h2>
        <span class="pull-right">
        	<a href="<?php echo BASE_URL;?>cloud/addProduto" class="btn btn-success">Adicionar Novo</a>
        </span>
    </div>
</div>

<div class="row">
	<table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Editar/Excluir</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($products as $item):?>

                <tr>
                    <td><?php echo $item['id'];?></td>
                    <td><?php echo $item['name'];?></td>
                    <td><a href="<?php echo BASE_URL;?>cloud/editarProduto/<?php echo $item['id'];?>" class="btn btn-default">EDITAR</a>
                    	<a href="<?php echo BASE_URL;?>cloud/excluirProduto/<?php echo $item['id'];?>" class="btn btn-danger" onclick="return confirm('Você tem certeza que quer excluir este produto?')">EXCLUIR</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
</div>