<div class="row">
                        <div class="col-xs-12">
                <div class="checkout-box m-top30">
                    <div class="row cart-bottom-buffer-md paymentwrap">
                        <div class="col-md-12 header">
                            <h2 style="color: #de058e;">
                                Boleto
                            </h2>

                            <div id="payment_tabs">
                                                                   

                                    <div class="tab-content">                                                                   <div id="payment-pagseguro_app" class="tab-pane payment-method inactive" style="height: auto;">

    
            <br>
            
    </div>
</div>
</div>                                                                                <div id="payment-paghiper" class="tab-pane payment-method inactive active" style="height: auto;">
    <style type="text/css">
    .boleto_option label,
    .boleto_option span,
    .boleto_option div,
    .boleto_option p{
        font-family: 'Arial';
    }
    .boleto_option .well{
        padding: 40px 0;
    }
    .paghiper-type-header{
        color: #666;
        font-size: 20px;
    }
    .paghiper-type-list{
        color: #F37B20;
        list-style: outside none disc;
        line-height: 145%;
        margin-left: 15px;
        padding: 10px 0px;
    }
    .paghiper-type-list li{
        font-size: 18px;
        font-weight: 300;
        margin-bottom: 15px;
    }
    .paghiper-type-warning{
        color: #666;
        font-size: 16px;
    }
    .paghiper-barcode{
        color: #666;
        font-weight: 600;
        text-align: center;
    }
    .paghiper-barcode .glyphicon-barcode{
        font-size: 35px;
    }
    .paghiper-type-prize-in{
        background: #09A51C none repeat scroll 0% 0%;
        color: #FFF;
        padding: 5px;
    }
</style>
<div class="row">
    <div class="col-md-12 boleto_option">
        <div class="well" style="background-color: #fff;">
            <div class="row" >
                <div class="col-md-2 col-xs-12 paghiper-barcode">
                    <i class="glyphicon glyphicon-barcode"></i>
                    <p>boleto</p>
                </div>
                <div class="col-md-9 col-xs-12">
                    <p class="paghiper-type-header">
                        Pagamento à vista
                    </p>
                    
                    <ul class="paghiper-type-list">
                        <li>
                            Importante: Verifique a data de vencimento informada no boleto.
                            Em caso de não pagamento do boleto até a data de vencimento, o pedido
                            será automaticamente cancelado.
                        </li>
                        <li>
                            O prazo de entrega informado durante a compra é contado a partir da confirmação
                            do pagamento pelo banco, o que pode levar até 3 dias úteis.
                        </li>
                        <li>
                            O pagamento do boleto pode ser efetuado pela internet, utilizando o código de barras,
                            ou diretamente em bancos, lotéricas e correios, apresentando o boleto impresso.
                        </li>
                    </ul>
                    <div class="paghiper-type-warning">
                        <strong>Atenção:</strong>
                        Você será redirecionado para a página do Boleto ao clicar em "Finalizar compra".
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                                            </div>
                                                                            </div>

                                                            </div>
                        </div>
                    </div>
                </div>

                                <div class="row">
                    <div class="col-xs-12">
                        <div class="cart-bottom-buffer-md text-right cart-country">
                            Esta compra está sendo feita no
                            <strong>Brasil</strong>
                            <span class="country-flag">
                                <img src="https://mist-store.xtechcommerce.com/loja/themes/default/assets/img/br.png" alt="Brasil">
                            </span>
                        </div>
                    </div>
                </div>
                
                <form method="POST">
                    <input type="hidden" name="name" value="<?php echo $user['name']." ".$user['nick']; ?>">
                    <input type="hidden" name="cpf" value="<?php echo $user['cpf']; ?>">
                    <input type="hidden" name="telefone" value="<?php echo $user['telefone']; ?>">
                    <input type="hidden" name="cep" value="<?php echo $user['cep']; ?>">
                    <input type="hidden" name="rua" value="<?php echo $user['endereco']; ?>">
                    <input type="hidden" name="numero" value="<?php echo $user['numero']; ?>">
                    <input type="hidden" name="complemento" value="<?php echo $user['complemento']; ?>">
                    <input type="hidden" name="bairro" value="<?php echo $user['bairro']; ?>">
                    <input type="hidden" name="cidade" value="<?php echo $user['cidade']; ?>">
                    <input type="hidden" name="estado" value="<?php echo $user['estado']; ?>">


                    <div class="row cart-bottom-buffer-md text-center cart-actions">
                    <button type="submit" class="btn btn-lg btn-primary" style="background-color: #de058e;">
                        <i class="glyphicon glyphicon-ok"></i> Finalizar compra
                    </button>
                </div>
                </form>
                
            </div>
        </div>