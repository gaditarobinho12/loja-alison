<?php $products = new Products();?>
<div class="myaccount row">
    <div class="col-xs-3 sidebar">
        <div class="side-header" style="background-color: #de048e;">
            <i class="glyphicon glyphicon-star"></i> Minha conta
        </div>

        <ul id="mytabs">
            <li class=""><a href="#dashboard" data-toggle="tab">Painel principal</a></li>

            <li><a href="#myaccount" data-toggle="tab">Dados pessoais</a></li>

            <li><a href="#addresses" data-toggle="tab">Endereço</a></li>

            <li><a href="#myorders" data-toggle="tab">Histórico de pedidos</a></li>

            <li><a href="#mywishlist" data-toggle="tab">Lista de desejos</a></li>
        </ul>
    </div>
<div class="col-xs-9 main">
<div class="tab-content">
    <div class="tab-pane active" id="dashboard">
    <h2 class="category-header" style="color: #de058e;">Painel principal</h2>

    <p>Bem vindo (a) <strong><?php echo $viewData['user']['name'];?></strong>! No painel principal você pode visualizar um resumo das atividades mais recentes da sua conta e atualizar suas informações de conta. Selecione um link abaixo para visualizar ou editar informações.</p>

        <div class="row">
        <div class="col-xs-6">
        <div class="inbox">
        <h4>
        Informações da conta
        <span class="pull-right">
        <a href="#myaccount" data-toggle="tab">
        Editar
        </a>
        </span>
        </h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">
        <ul>
        <?php echo $viewData['user']['name']; ?> <?php echo $viewData['user']['nick']; ?><br>
        <?php echo $viewData['user']['email']; ?><br>
        <?php echo $viewData['user']['telefone']; ?>
        </ul>
        </div>
        </div>
        </div>

        <div class="col-xs-6">
        <div class="inbox">
        <h4>
        Endereço para entrega
        <span class="pull-right">
        <a href="<?php echo BASE_URL;?>users/#updateAddress" id="list_all" class="btn_link">
        
        </a>
        </span>
        </h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">
        <ul>
        <li><?php echo $viewData['user']['name']; ?> <?php echo $viewData['user']['nick']; ?><br>
        <?php echo $viewData['user']['endereco']; ?> <?php echo $viewData['user']['numero']; ?><br>
        <?php echo $viewData['user']['complemento']; ?><br>
        <?php echo $viewData['user']['cidade']; ?>, <?php echo $viewData['user']['bairro']; ?>, <?php echo $viewData['user']['estado']; ?> <?php echo $viewData['user']['cep']; ?><br>
        Brasil<br>
        </li>
        </ul>
        </div>
        </div>
        </div>
        </div>
    </div>

<div class="tab-pane" id="myaccount">
    <h2 class="category-header" style="color: #de058e;">Dados pessoais</h2>

    <form class="form-horizontal" method="POST" accept-charset="utf-8" >
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Dados para acesso</h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">
        <div class="alert alert-info">
        <i class="glyphicon glyphicon-info-sign"></i>
        Se você não quiser alterar sua senha, deixe os campos em branco.
        </div>

        <div class="form-group">
        <label for="email" class="col-xs-3 control-label">
        E-mail *
        </label>
        <div class="col-xs-5">
        <input type="text" name="" value="<?php echo $viewData['user']['email']; ?>" placeholder="E-mail" class="form-control" autocomplete="off" disabled>
        <input type="hidden" name="emailU" value="<?php echo $viewData['user']['email']; ?>">
        </div>
        </div>
        <div class="form-group">
        <label for="password" class="col-xs-3 control-label">
        Senha
        </label>
        <div class="col-xs-5">
        <input type="password" name="password" value="" placeholder="Senha" class="form-control" autocomplete="off">
        </div>
        </div>
        <div class="form-group">
        <label for="confirm" class="col-xs-3 control-label">
        Confirmar senha
        </label>
        <div class="col-xs-5">
        <input type="password" name="confirm" value="" placeholder="Confirmar senha" class="form-control" autocomplete="off">
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>

        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Dados pessoais</h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome *
        </label>
        <div class="col-xs-5">
        <input type="text" name="firstname" value="<?php echo $viewData['user']['name']; ?>" placeholder="Nome" class="form-control" requred>
        </div>
        </div>

        <div class="form-group">
        <label for="lastname" class="col-xs-3 control-label">
        Sobrenome *
        </label>
        <div class="col-xs-5">
        <input type="text" name="lastname" value="<?php echo $viewData['user']['nick']; ?>" placeholder="Sobrenome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="cpf" class="col-xs-3 control-label">
        CPF *
        </label>
        <div class="col-xs-5">
        <input type="text" name="cpf" value="<?php echo $viewData['user']['cpf']; ?>" placeholder="CPF" class="form-control" disabled>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Telefone
        </label>
        <div class="col-xs-5">
        <input type="text" name="phone" value="<?php echo $viewData['user']['telefone']; ?>" placeholder="Telefone" class="form-control" required>
        </div>

        </div>
        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck" style="background-color: #de058e;">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>
</div>

<div class="tab-pane" id="addresses">
<h2 class="category-header" style="color: #de058e;">Seu Endereço</h2>

<div class="row">
<div class="col-xs-12">
<div class="inbox">
<h4>Dados de entrega</h4>
</div>

<div class="panel panel-default">
<table class="table table-striped">
<tbody><tr id="">
<td>
<?php echo $viewData['user']['name']." ".$viewData['user']['nick'];?> <br>
<?php echo $viewData['user']['endereco']." ".$viewData['user']['numero']?><br>
<?php echo $viewData['user']['complemento']?><br>
<?php echo $viewData['user']['cidade']?>, <?php echo $viewData['user']['bairro']?>, <?php echo $viewData['user']['cep'];?><br>
Brasil<br>

</td>
<td>
<div class="row">
<div class="col-xs-12">
<div class="btn-group pull-right">
<a href="#updateAddress" data-toggle="tab" class="btn btn-default">Editar</a>
</div>
</div>
</div>

</td>
</tr>
</tbody></table>

</div>
</div>
</div>
</div>

<div class="tab-pane" id="updateAddress">
<h2 class="category-header" style="color: #de058e;">Edite seu Endereço</h2>

<div class="row">
<div class="col-xs-12">
<form class="form-horizontal" method="POST" accept-charset="utf-8" >
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Dados de entrega</h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome *
        </label>
        <div class="col-xs-5">
        <input type="text" name="nome_entrega" value="<?php echo $viewData['user']['name']; ?>" placeholder="Nome" class="form-control" requred>
        </div>
        </div>

        <div class="form-group">
        <label for="lastname" class="col-xs-3 control-label">
        Sobrenome *
        </label>
        <div class="col-xs-5">
        <input type="text" name="sobrenome" value="<?php echo $viewData['user']['nick']; ?>" placeholder="Sobrenome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Rua
        </label>
        <div class="col-xs-5">
        <input type="text" name="rua_entrega" value="<?php echo $viewData['user']['endereco']; ?>" placeholder="Rua" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Número
        </label>
        <div class="col-xs-5">
        <input type="text" name="numero_entrega" value="<?php echo $viewData['user']['numero']; ?>" placeholder="Numero" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Complemento
        </label>
        <div class="col-xs-5">
        <input type="text" name="complemento_entrega" value="<?php echo $viewData['user']['complemento']; ?>" placeholder="Complemento" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Cidade
        </label>
        <div class="col-xs-5">
        <input type="text" name="cidade_entrega" value="<?php echo $viewData['user']['cidade']; ?>" placeholder="Cidade" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Bairro
        </label>
        <div class="col-xs-5">
        <input type="text" name="bairro_entrega" value="<?php echo $viewData['user']['bairro']; ?>" placeholder="Bairro" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Estado:</label>
                <div class="col-xs-5">
                    <select name="estado_entrega" id="uf" class="form-control" required>
    <option value="" <?=($user['estado'] == '')?'selected':''?>>Selecionar</option>
    <option value="AC" <?=($user['estado'] == 'AC')?'selected':''?>>Acre</option>
    <option value="AL" <?=($user['estado'] == 'AL')?'selected':''?>>Alagoas</option>
    <option value="AP" <?=($user['estado'] == 'AP')?'selected':''?>>Amapá</option>
    <option value="AM" <?=($user['estado'] == 'AM')?'selected':''?>>Amazonas</option>
    <option value="BA" <?=($user['estado'] == 'BA')?'selected':''?>>Bahia</option>
    <option value="CE" <?=($user['estado'] == 'CE')?'selected':''?>>Ceará</option>
    <option value="DF" <?=($user['estado'] == 'DF')?'selected':''?>>Distrito Federal</option>
    <option value="ES" <?=($user['estado'] == 'ES')?'selected':''?>>Espírito Santo</option>
    <option value="GO" <?=($user['estado'] == 'GO')?'selected':''?>>Goiás</option>
    <option value="MA" <?=($user['estado'] == 'MA')?'selected':''?>>Maranhão</option>
    <option value="MT" <?=($user['estado'] == 'MT')?'selected':''?>>Mato Grosso</option>
    <option value="MS" <?=($user['estado'] == 'MS')?'selected':''?>>Mato Grosso do Sul</option>
    <option value="MG" <?=($user['estado'] == 'MG')?'selected':''?>>Minas Gerais</option>
    <option value="PA" <?=($user['estado'] == 'PA')?'selected':''?>>Pará</option>
    <option value="PB" <?=($user['estado'] == 'PB')?'selected':''?>>Paraíba</option>
    <option value="PR" <?=($user['estado'] == 'PR')?'selected':''?>>Paraná</option>
    <option value="PE" <?=($user['estado'] == 'PE')?'selected':''?>>Pernambuco</option>
    <option value="PI" <?=($user['estado'] == 'PI')?'selected':''?>>Piauí</option>
    <option value="RJ" <?=($user['estado'] == 'RJ')?'selected':''?>>Rio de Janeiro</option>
    <option value="RN" <?=($user['estado'] == 'RN')?'selected':''?>>Rio Grande do Norte</option>
    <option value="RS" <?=($user['estado'] == 'RS')?'selected':''?>>Rio Grande do Sul</option>
    <option value="RO" <?=($user['estado'] == 'RO')?'selected':''?>>Rondônia</option>
    <option value="RR" <?=($user['estado'] == 'RR')?'selected':''?>>Roraima</option>
    <option value="SC" <?=($user['estado'] == 'SC')?'selected':''?>>Santa Catarina</option>
    <option value="SP" <?=($user['estado'] == 'SP')?'selected':''?>>São Paulo</option>
    <option value="SE" <?=($user['estado'] == 'SE')?'selected':''?>>Sergipe</option>
    <option value="TO" <?=($user['estado'] == 'TO')?'selected':''?>>Tocantins</option>
    </select>
                </div>
        </div>

        

        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        CEP
        </label>
        <div class="col-xs-5">
        <input type="text" name="cep_entrega" value="<?php echo $viewData['user']['cep']; ?>" placeholder="CEP" class="form-control" required>
        </div>
        </div>



        <div class="form-group">
        <label for="telephone" class="col-xs-3 control-label">
        Telefone
        </label>
        <div class="col-xs-5">
        <input type="text" name="phone" value="<?php echo $viewData['user']['telefone']; ?>" placeholder="Telefone" class="form-control" required>
        </div>

        </div>
        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck" style="background-color: #de058e;">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>

    

</div>
</div>
</div>

<div class="tab-pane" id="myorders">
<h2 class="category-header" style="color: #de058e;">Histórico de pedidos</h2>
<div class="row">
<div class="col-xs-12">
<div class="inbox">
<h4>Pedidos</h4>
</div>
<div class="panel panel-default" >
    
        <table class="table table-striped table-hover">
            <thead >
                <tr>
                    <th style="background-color: #de058e;">Pedido de</th>
                    <th style="background-color: #de058e;">Número do pedido</th>
                    <th style="text-align:center; background-color: #de058e;">Status do pedido</th>
                    <th style="text-align:center; background-color: #de058e;">Ver pedido</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pedidos as $item):?>
                <tr>
                    <td>
                    <?php echo $item['data'];?>
                    </td>
                    <td>
                    <a href="">
                    <?php echo $item['id'];?>
                    </a>
                    </td>
                    <td align="center">
                    <?php if($item['payment_status'] == 1):?>
                            <span class="label label-warning">Aguardando Pagamento</span>
                        <?php elseif($item['payment_status'] == 2):?>
                            <span class="label label-info">Em análise</span>
                        <?php elseif($item['payment_status'] == 3):?>
                            <span class="label label-success">Pagamento Aprovado</span>
                        <?php elseif($item['payment_status'] == 7):?>
                            <span class="label label-danger">Compra cancelada</span>
                        <?php endif;?>
                    </td>
                    <td align="center">
                    <a href="<?php echo BASE_URL;?>pedido/open/<?php echo $item['id'];?>" class="btn btn-sm btn-success" style="background-color: #de058e; border-color: #de058e;">
                    Abrir Pedido
                    </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    
</div>
</div>
</div>
</div>

<div class="tab-pane" id="mywishlist">
<h2 class="category-header" style="color: #de058e;">Lista de desejos</h2>

<div class="row">
<div class="col-xs-12">
<?php foreach($viewData['wishList'] as $item):?>
    <?php $infoP = $products->getInfo($item['id_product']);?>
    <div class="row">
<div class="col-xs-3">
<img src="<?php echo BASE_URL;?>media/products/<?php echo $infoP['image'];?>" alt="<?php echo $infoP['name'];?>" title="" class="thumbnail">
</div>
<div class="col-xs-9">
<h4><?php echo $infoP['name'];?></h4>
<strong class="buy-price">

<span class="sale"><?php echo "R$ ".number_format($infoP['price'],2, ",", ".");?></span>
</strong>
<p class="buy-fields">
</p>

<a href="<?php BASE_URL;?>product/open/<?php echo $item['id_product'];?>" class="btn btn-primary buy-btn" type="submit" value="submit" style="background-color: #de058e;">
    <i class="glyphicon glyphicon-eye-open"></i> Ver Produto
</a> 
ou <a href="<?php echo BASE_URL?>wishlist/del/<?php echo $item['id'];?>">Excluir</a>

<p></p>
</div>
</div>
<?php endforeach;?>
<hr>
</div>
</div>
</div>

</div>
</div>
</div>