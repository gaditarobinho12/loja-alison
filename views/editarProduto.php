<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Editar Produto
        </h2>
    </div>
</div>

<div class="row">
<div class="col-xs-12">
<form class="form-horizontal" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Atualizar produto</h4>
        </div>
        <?php if(isset($avisosS)):?>
            <div class="alert alert-success"><?php echo $avisosS?></div>
        <?php endif;?>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome do produto *
        </label>
        <div class="col-xs-5">
        <input type="text" name="nome" value="<?php echo $productInfo['name']; ?>" placeholder="Nome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Descrição do produto *
        </label>
        <div class="col-xs-5">
        <textarea name="descricao" class="form-control" required=""><?php echo $productInfo['description'] ?></textarea>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Preço atual *
        </label>
        <div class="col-xs-5">
        <input type="text" name="preco" value="<?php echo $productInfo['price']; ?>" placeholder="Nome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Preço anterior *
        </label>
        <div class="col-xs-5">
        <input type="text" name="preco_anterior" value="<?php echo $productInfo['price_from']; ?>" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Tamanhos disponíveis*
        </label>
        <div class="col-xs-5">
        <input type="text" name="size" class="form-control">
        </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Categoria:</label>
                <div class="col-xs-5">
                    <select name="categoria" id="uf" class="form-control">
                        <?php foreach($cats as $cat):?>
                            <option value="<?php echo $cat['id'];?>" <?=($cat['id'] == $productInfo['id_category'])?'selected':''?>><?php echo $cat['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Marca:</label>
                <div class="col-xs-5">
                    <select name="marca" id="uf" class="form-control">
                        <?php foreach($marcas as $marca):?>
                            <option value="<?php echo $marca['id'];?>" <?=($marca['id'] == $productInfo['id_brand'])?'selected':''?>><?php echo $marca['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Quantidade em estoque *
        </label>
        <div class="col-xs-5">
        <input type="text" name="estoque" value="<?php echo $productInfo['stock']; ?>" class="form-control" required>
        </div>
        </div>


        
        <?php $this->loadView('tamanhos',[
            'size'=>$size
        ]); ?>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Peso (Kg)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="peso" value="<?php echo $productInfo['weight']; ?>" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Largura (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="largura" value="<?php echo $productInfo['width']; ?>" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Atura (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="altura" value="<?php echo $productInfo['height']; ?>" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Comprimento (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="comprimento" value="<?php echo $productInfo['lenght']; ?>" class="form-control">
        </div>
        </div>

        <div class="form-group">
            <label for="add_foto" class="col-xs-3 control-label">Fotos do produto</label>

            <div class="col-xs-5">
                <input type="file" name="fotos[]" multiple class="form-control">
<br>
                <div class="panel panel-default">
                    <div class="panel-heading">Fotos do anuncio</div>
                    <div class="panel-body">
                        <?php //print_r($productInfo['fotos']);exit;?>
                        <?php foreach($productInfo['fotos'] as $item):?>
                            <div class="foto_item col-xs-4">
                                <img src="<?php echo BASE_URL?>media/products/<?php echo $item['url'];?>" class="img-thumbnail"><br>
                                <a href="<?php echo BASE_URL?>cloud/excluirFoto/<?php echo $item['id'];?>" class="btn btn-danger">EXCLUIR</a>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>

        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>

    

</div>
</div>