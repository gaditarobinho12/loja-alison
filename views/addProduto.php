<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Adicionar Produto
        </h2>
    </div>
</div>

<div class="row">
<div class="col-xs-12">
<form class="form-horizontal" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Inserir produto</h4>
        </div>
        <?php if(isset($avisosS)):?>
            <div class="alert alert-success"><?php echo $avisosS?></div>
        <?php endif;?>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome do produto *
        </label>
        <div class="col-xs-5">
        <input type="text" name="nome" value="" placeholder="Nome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Descrição do produto *
        </label>
        <div class="col-xs-5">
        <textarea name="descricao" class="form-control" required=""></textarea>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Preço atual *
        </label>
        <div class="col-xs-5">
        <input type="text" name="preco" value="" placeholder="Nome" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Preço anterior *
        </label>
        <div class="col-xs-5">
        <input type="text" name="preco_anterior" value="" class="form-control" required>
        </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Categoria:</label>
                <div class="col-xs-5">
                    <select name="categoria" id="uf" class="form-control">
                        <?php foreach($cats as $cat):?>
                            <option value="<?php echo $cat['id'];?>"><?php echo $cat['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Marca:</label>
                <div class="col-xs-5">
                    <select name="marca" id="uf" class="form-control">
                        <?php foreach($marcas as $marca):?>
                            <option value="<?php echo $marca['id'];?>"><?php echo $marca['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </div>
        </div>


        <?php $this->loadView('tamanhos',[
            'size'=>[]
        ]); ?>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Peso (Kg)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="peso" value="" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Largura (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="largura" value="" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Atura (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="altura" value="" class="form-control">
        </div>
        </div>

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Comprimento (cm)*
        </label>
        <div class="col-xs-5">
        <input type="text" name="comprimento" value="" class="form-control">
        </div>
        </div>

        <div class="form-group">
            <label for="add_foto" class="col-xs-3 control-label">Fotos do produto</label>

            <div class="col-xs-5">
                <input type="file" name="fotos[]" multiple class="form-control">
            </div>
        </div>

        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>

    

</div>
</div>