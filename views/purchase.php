<?php $c = new Cloud();?>
<div class="col-sm-8">
	<h1>Itens do Pedido</h1>
	<table class="table table-striped table-hover">
  <thead>
  	<tr>
    	<th>Produto</th>
    	<th>Quantidade</th>
    	<th>Tamanho</th>
  	</tr>
  </thead>

  <tbody>
  	<?php foreach($itensPedidos as $item):?>
  		<?php $produto = $c->getDadosProduct($item['id_product']);?>
  	<tr>
    	<td><a href="<?php echo BASE_URL;?>product/open/<?php echo $item['id_product'];?>" target="_blank"><?php echo utf8_encode($produto['name']);?></a></td>
    	<td><?php echo $item['quantity'];?></td>
    	<td><?php echo $item['size'];?></td>
    	
  	</tr>
  	<?php endforeach;?>
  </tbody>
  
</table>
</div>

<div class="col-sm-4">
	<h1>Resumo do pedido</h1>
	<strong>Nome do Cliente:</strong> <?php echo $cliente['name'];?><br><br>
	<strong>Tipo de pagamento:</strong> <?php echo $dataPedidos['payment_type'];?><br><br>
	<strong>Status do Pagamento:</strong>  
	<?php if($dataPedidos['payment_status'] == 1):?>
			<span class="label label-warning">Aguardando Pagamento</span>
		<?php elseif($dataPedidos['payment_status'] == 2):?>
			<span class="label label-info">Em análise</span>
		<?php elseif($dataPedidos['payment_status'] == 3):?>
			<span class="label label-success">Pagamento Aprovado</span>
		<?php elseif($dataPedidos['payment_status'] == 7):?>
			<span class="label label-danger">Compra cancelada</span>
	<?php endif;?>
	<br><br>
	<strong>Data:</strong> <?php echo $dataPedidos['data'];?><br><br>
	<strong>Total: </strong><?php echo "R$".number_format($dataPedidos['total_amount'],2, ",", ".");?>

        <?php if($dataPedidos['payment_status'] != 1 && $dataPedidos['payment_status'] != 2):?>
          <br><br>
          <form method="POST">
            <label for="nome">Cod.Objeto:</label>
            <input type="text" class="form-control" name="objeto" value="<?php echo $dataPedidos['objeto'];?>">
            <button class="btn btn-info">Inserir</button>
          </form>
        <?php endif;?>

</div>
<div style="clear: both;"></div>
<hr>

<h1>Dados de entrega:</h1>
<div class="form-group col-sm-6">
        <label for="nome">Nome de entrega:</label>
        <input type="text" class="form-control" id="cep" name="cep" value="<?php echo $cliente['name']." ".$cliente['nick'];?>" disabled>
      </div>

<div class="form-group col-sm-6">
        <label for="nome">Cep:</label>
        <input type="text" class="form-control" id="cep" name="cep" value="<?php echo $cliente['cep'];?>" disabled>
      </div>

<div class="form-group col-sm-8">
        <label for="nome">Endereço:</label>
        <input type="text" class="form-control" id="rua" name="rua" value="<?php echo $cliente['endereco'];?>" disabled>
      </div>

<div class="form-group col-sm-4">
        <label for="nome">Numero:</label>
        <input type="text" class="form-control" id="numero" name="numero" value="<?php echo $cliente['numero'];?>" disabled>
      </div>

<div class="form-group col-sm-6">
        <label for="nome">Complemento:</label>
        <input type="text" class="form-control" id="nome" name="complemento" value="<?php echo $cliente['complemento'];?>" disabled>
      </div>

<div class="form-group col-sm-6">
        <label for="nome">Bairro:</label>
        <input type="text" class="form-control" id="bairro" name="bairro" value="<?php echo $cliente['bairro'];?>" disabled>
      </div>

<div class="form-group col-sm-6">
        <label for="nome">Cidade:</label>
        <input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo $cliente['cidade'];?>" disabled>
      </div>

<div class="form-group col-sm-6">
	<label for="nome">Estado:</label>
	<input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo $cliente['estado'];?>" disabled>
</div>