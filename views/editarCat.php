<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Editar Categoria
        </h2>
    </div>
</div>

<div class="row">
<div class="col-xs-12">
<form class="form-horizontal" method="POST" accept-charset="utf-8" >
        <div class="row">
        <div class="col-xs-12">
        <div class="inbox">
        <h4>Atualizar categoria</h4>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">

        <div class="form-group">
        <label for="firstname" class="col-xs-3 control-label">
        Nome da categoria *
        </label>
        <div class="col-xs-5">
        <input type="text" name="nome_cat" value="<?php echo $catInfo['name']; ?>" placeholder="Nome" class="form-control" requred>
        </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-xs-3 control-label">Categoria Pai:</label>
                <div class="col-xs-5">
                    <select name="cat_pai" id="uf" class="form-control">
                    	<option value="<?php $v = NULL; echo $v;?>" <?=($catInfo['sub'] == 'NULL')?'selected':''?>>Próprio pai</option>

				    	<?php foreach($cats as $cat):?>
				    		<option value="<?php echo $cat['id'];?>" <?=($catInfo['sub'] == $cat['sub'])?'selected':''?>><?php echo $cat['name'];?></option>
				    	<?php endforeach;?>
				    </select>
                </div>
        </div>
        <span class="pull-right">
        <input type="submit" value="Atualizar" class="btn btn-primary btn-bck">
        </span>
        </div>
        </div>
        </div>
        </div>

    </form>

    

</div>
</div>