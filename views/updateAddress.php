<div class="myaccount row">
    <div class="col-xs-3 sidebar">
        <div class="side-header">
            <i class="glyphicon glyphicon-star"></i> Minha conta
        </div>

        <ul id="mytabs">
            <li class=""><a href="<?php echo BASE_URL;?>#dashboard" data-toggle="tab">Painel principal</a></li>

            <li><a href="#myaccount" data-toggle="tab">Dados pessoais</a></li>

            <li><a href="#addresses" data-toggle="tab">Endereço</a></li>

            <li><a href="#myorders" data-toggle="tab">Histórico de pedidos</a></li>

            <li><a href="#mytestimonials" data-toggle="tab">Depoimentos</a></li>

            <li><a href="#mywishlist" data-toggle="tab">Lista de desejos</a></li>
        </ul>
    </div>