<?php
class estaticsController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }


    public function perguntasFrequentes() {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $f = new Filters();
        $u = new Users();

        $dados = $store->getTemplateData();

        $filters = array();
        if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
            $filters = $_GET['filter'];
        }

        $dados['user'] = $u->getDados();

        

        //$dados['sidebar'] = true;
        //$dados['slider'] = true;

        $this->loadTemplate('perguntas-frequentes', $dados);
    }

}