<section id="store-content">
    		<div class="container">
    		    <div class="inner-content">
        			
    <div id="cartview">
            <?php if(empty($_SESSION['cart'])):?>
                <div class="alert alert-info alert-no-prod">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Não há produtos no seu carrinho!
                </div>
            <?php else:?>                    

            <link href="https://cdn.xtechcommerce.com/loja/themes/default/assets/css/view_cart.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="https://cdn.xtechcommerce.com/loja/themes/default/assets/js/shop-v11.js"></script>
        <script type="text/javascript" src="https://cdn.xtechcommerce.com/loja/themes/default/assets/js/cpf-cnpj-validator.js"></script>
        

        <div class="row">
            <div class="page-header text-left">
                <a href="<?php echo BASE_URL;?>" class="btn btn-default cart_contBuy pull-left" style="color: #de058e; border-color: #de058e;">Continuar Comprando</a>
                <h2>Seu carrinho</h2>
               <a href="<?php echo BASE_URL;?>cart/clear" class="btn btn-default cart_finish pull-right" style="background-color: #de058e; border-color: #de058e;">Limpar Carrinho</a>
            </div>
        </div>
            
        <div>
        <div class="custom-alerts">
            </div>

                <div class="summary">
                    <div id="checkout_summary">
                        <form action="<?php echo BASE_URL;?>cart/payment_redirect" method="post" accept-charset="utf-8" id="update_cart_form">
                            <input type="hidden" name="step" value="">
                            <div class="table-responsive">
            <table class="table table-striped table-condensed">
                <thead style="background-color: #de058e;">
                    <tr>
                        <th colspan="2">
                            Produto
                        </th>
                        <th style="width: 15%;">
                            Preço
                        </th>
                        <th style="width: 12%;">
                            Qtd.
                        </th>
                        <th style="width: 15%;">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>
<?php $subtotal = 0;?>
    <?php foreach($list as $item): ?>
    <?php

    $subtotal += (floatval($item['price']) * intval($item['qt']));
    ?>
<tr>

<td width="7%" class="cart-product-img">
    <img src="<?php echo BASE_URL; ?>media/products/<?php echo $item['image']; ?>" alt="" title="" width="78">
</td>

<td class="cart-product-name" data-slug="Bota-Miley-Low-Rocha">
    <strong><?php echo $item['name']; ?></strong>
    <div>
    Numeração:<?php echo $item['size']['text_size']; ?>
    </div>
</td>

<td>
            <span class="normal"><?php echo "R$".number_format($item['price'], 2, ',', '.'); ?></span>
    </td>
<td>
<div class="control-group">
<div class="controls">
<div class="input-group text-center">
<?php if($item['qt'] > 1):?>
    <a href="<?php echo BASE_URL;?>cart/decrement/<?php echo $item['id'];?>/<?php echo $item['size']['id_option'];?>" class="btn-quantity btn-minus">
        <span class="glyphicon glyphicon-chevron-down"></span>
    </a>
<?php elseif($item['qt'] = 1):?>
    <a href="<?php echo BASE_URL;?>cart/del/<?php echo $item['id'];?>/<?php echo $item['size']['id_option'];?>" class="btn-quantity btn-minus">
        <span class="glyphicon glyphicon-chevron-down"></span>
    </a>
<?php endif;?>


<a href="<?php echo BASE_URL;?>cart/increment/<?php echo $item['id'];?>/<?php echo $item['size']['id_option'];?>" class="btn-quantity btn-plus">
<span class="glyphicon glyphicon-chevron-up"></span>
</a>

<input class="form-control text-center" name="" value="<?php echo $item['qt'];?>" style="width: 50% !important;" disabled>                                                                                                                </div>
</div>
</div>
            
<a class="remov_prod center" href="#" onclick="javascript: if(confirm('Tem certeza que deseja remover este item do seu carrinho?')){ window.location='<?php echo BASE_URL; ?>cart/del/<?php echo $item['id']; ?>/<?php echo $item['size']['id_option'];?>'; }">Remover</a>
</td>
<td>
    <?php echo "R$".number_format(($item['price'] * $item['qt']), 2, ',', '.'); ?>
</td>
</tr>
<?php endforeach;?>
<?php
    if(isset($_SESSION['shipping'])){
        $frete = floatval(str_replace(',', '.', $shipping['price']));
    } else{
        $frete = 0;
    }
    $subtotal = $subtotal - ($subtotal * $_SESSION['desconto'] / 100);
    $total = ($subtotal + $frete);
?>
</tbody>
                <tfoot>
                                    <tr>
                        <td colspan="4" class="text-right">
                            <strong>
                                Subtotal
                            </strong>
                        </td>
                        <td>
                            <?php echo "R$".number_format($subtotal, 2, ',', '.'); ?>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" class="text-right">
                            <strong>
                                Total
                            </strong>
                        </td>
                        <td>
                            <?php echo "R$".number_format($total, 2, ',', '.'); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
    </div>
</form>

<script>
    function calcQuantity(oObj,type){
        var input = $(oObj).closest('.input-group').find('input');
        if (type == "minus"){
            var final = parseFloat(input.val()) - 1;
        }else if  (type == "plus"){
            var final = parseFloat(input.val()) + 1;
        }
        if (final < 1){ final = 1; }
        input.val(final);
        input.change();
    }
</script>
        </div>
        <div class="row cart-summary-bottom" style="background-color: #de058e;">
            <div class="cart-secure col-md-6 pull-right">
                <ul class="list-inline pull-right">
                    <li><span class="glyphicon glyphicon-lock"></span> Compra segura</li>
                    <li><span class="glyphicon glyphicon-home"></span> Entrega garantida</li>
                </ul>
            </div>
        </div>

                    <div class="row cart-functions" id="checkout">
<div class="col-md-4">
<div class="panel panel-default panel-checkout">
<div class="panel-heading" style="background-color: #de058e;"><i class="fa fa-truck"></i>Calcule o Frete</div>
<div class="panel-body">                                                                                                                                        
<div class="input-group">

<form method="POST" class="update_cart_form_coupon">
    <input type="text" name="cep" id="zip_code" class="address form-control">
<span class="input-group-btn input-group-search-address">
<input type="submit" class="btn btn-default btn-search-address pull-right" value="Calcular Frete">
</form>
</span>
</div>
<div class="shippingwrap">
<table id="shipping_table">   

<?php if(isset($shipping['price'])): ?>
<tr>
    <td>
        Valor: <strong><?php echo "R$".$shipping['price']; ?></strong> <span class="label label-info">Entrega em torno de <?php echo $shipping['date']; ?> Dia <?php echo ($shipping['date'] > 1)?'s úteis':'útil'?></span>        
    </td>
</tr>
<?php endif;?>

<tr><td colspan="3" style="padding: 10px;"></td></tr>
<tr><td colspan="3" class="shipping-method-error alert alert-danger" style="display: none;"></td></tr>
</table>
</div>
</div>
</div>
</div>

    <div class="col-md-4">
        <div class="panel panel-default panel-checkout">
          <div class="panel-heading" style="background-color: #de058e;"><i class="fa fa-ticket"></i>Insira seu Cupom</div>
          <div class="panel-body">
              <form method="POST" accept-charset="utf-8" id="update_cart_form_coupon">
                <div class="input-group ticket">
                    <input type="text" name="box_coupon_code" class="form-control" required>
                    <input type="submit" class="btn btn-default" value="Aplicar cupom">

                    <?php if(isset($verificaCode) && $verificaCode != 0):?>
                        <div class="alert alert-success">Cupom Adicionado com sucesso!</div>
                    <?php else:?>
                        <div class="alert alert-danger">Erro: O cupom não existe.</div>
                    <?php endif;?>
                    
                </div>
            </form>
          </div>
        </div>
    </div>

    <form method="POST" action="<?php echo BASE_URL;?>cart/payment_redirect">
        <div class="col-md-4">
        <div class="panel panel-default panel-checkout">
          <div class="panel-heading" style="background-color: #de058e;"><i class="fa fa-money"></i>Como deseja pagar?</div>
          <div class="panel-body">
              <div class="form-group col-sm-12">
                <select name="payment_type" class="form-control">
                <option value="checkout_transparente">Cartão de crédito</option>
                <option value="boleto">Boleto Bancário</option>
            </select>
            </div>
          </div>
        </div>
    </div>

        <div class="col-md-4 pull-right">
            <?php if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])):?>
            <input type="submit" value="Finalizar Compra" class="btn btn-default cart_finish width_full" style="background-color: #de058e;">
        <?php else:?>
            <a href="<?php echo BASE_URL;?>login" class="btn btn-default cart_finish pull-right" style="background-color: #de058e; border-color: #de058e;">Fazer Login</a>
        <?php endif;?>
        </div>
    </form>
</div>
                </div>
            </div>
            </div>
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('.cart-account-header .cart-account-buttons .pull-right').css('margin-top', '20px');
            $('.cart-coupon .btn-default').val('Aplicar');
            if(!$('#cartview .summary').length){
                $('#cartview .alert-no-prod').replaceWith('<p class="no-prod" style="margin-bottom: 20px;">Você ainda não colocou nenhum item no carrinho.');
            }
            
            // Retiro o hexadeciomal de cor da variacao
            $('#checkout_summary table tbody tr td:nth-child(2) div').each(function(){
                var html = $(this).html(), part = '';
                if(html.indexOf('Cor') === -1 || html.indexOf('Cores') === -1){
                    for(var i = 0, len = html.length; i < len; i++){
                        if(html[i] == '#'){
                            part = html.substr(html.indexOf('#'), 7);
                        }
                    }
                }
                $(this).html(html.replace(part, ''));
            });
        });
        
                
    </script>

                
                </div>
            <?php endif;?>
    </div>
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('.cart-account-header .cart-account-buttons .pull-right').css('margin-top', '20px');
    		$('.cart-coupon .btn-default').val('Aplicar');
    		if(!$('#cartview .summary').length){
                $('#cartview .alert-no-prod').replaceWith('<p class="no-prod" style="margin-bottom: 20px;">Você ainda não colocou nenhum item no carrinho.');
            }
    		
            // Retiro o hexadeciomal de cor da variacao
            $('#checkout_summary table tbody tr td:nth-child(2) div').each(function(){
                var html = $(this).html(), part = '';
                if(html.indexOf('Cor') === -1 || html.indexOf('Cores') === -1){
                    for(var i = 0, len = html.length; i < len; i++){
                        if(html[i] == '#'){
                            part = html.substr(html.indexOf('#'), 7);
                        }
                    }
                }
                $(this).html(html.replace(part, ''));
            });
    	});
    	
            	
    </script>

		        
    		    </div>
    		</div>
    	</section>