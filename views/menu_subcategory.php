<?php foreach($subs as $sub): ?>
	<li class="child">
    	<a id="filter_423953" class="sub" href="<?php echo BASE_URL.'categories/enter/'.$sub['id']; ?>"><?php echo $sub['name']; ?></a>
    </li>
	<?php
	if(count($sub['subs']) > 0) {
		$this->loadView('menu_subcategory', array(
			'subs' => $sub['subs'],
			'level' => $level + 1
		));
	}
	?>
<?php endforeach; ?>
