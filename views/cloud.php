<?php $c = new Cloud();?>
<div class="row">
    <div class="col-xs-12">
        <h2 class="category-header">
            Novos Pedidos
        </h2>
    </div>
</div>

<table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Cliente</th>
                    <th>Data</th>
                    <th>Total</th>
                    <th>Status do pagamento</th>
                    <th>Abrir</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pedidos as $item):?>
                    <?php 
                        $nome = $c->getDadosCliente($item['id_user']);
                    ?>
                <tr>
                    <td><?php echo $item['id'];?></td>
                    <td><?php echo $nome['name']." ".$nome['nick'];?></td>
                    <td><?php echo $item['data'];?></td>
                    <td><?php echo "R$".number_format($item['total_amount'],2, ",", ".");?></td>
                    <td>
                        <?php if($item['payment_status'] == 1):?>
            <span class="label label-warning">Aguardando Pagamento</span>
        <?php elseif($item['payment_status'] == 2):?>
            <span class="label label-info">Em análise</span>
        <?php elseif($item['payment_status'] == 3):?>
            <span class="label label-success">Pagamento Aprovado</span>
        <?php elseif($item['payment_status'] == 7):?>
            <span class="label label-danger">Compra cancelada</span>
    <?php endif;?>
                    </td>
                    <td><a href="<?php echo BASE_URL."cloud/open/".$item['id'];?>" class="btn btn-default">Abrir</a></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>