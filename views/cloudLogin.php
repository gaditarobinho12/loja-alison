<section id="store-content">
    		<div class="container">
    		    <div class="inner-content">
        			<div class="row">
    <div class="col-xs-12">
		<h2 class="page-title">Login Administrativo</h2>
		<hr>
	</div>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Entrar como ADM</h3>
            </div>
            <div class="panel-body">
                <form method="POST" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <input type="text" name="user" class="form-control" placeholder="Usuário" autocomplete="off" />
                    </div>
                    <div class="input-group margin-top">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input type="password" name="senha" class="form-control" placeholder="Sua senha" autocomplete="off" />
                    </div>
                    <div class="clearfix">
                        <label class="checkbox pull-right">
                            <input name="remember" value="true" type="checkbox" />
                            Continuar conectado
                        </label>
                        <!--<span class="margin-top10 pull-left">
                            <a href="forgot_password.html">
                                Esqueceu a senha?
                            </a>
                        </span>-->
                    </div>
                    <input type="submit" value="Login" name="submit" class="btn btn-primary margin-top pull-right" />
                </form>
            </div>
        </div>
    </div>
</div>

		        
    		    </div>
    		</div>
    	</section>