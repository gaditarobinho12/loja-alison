<section id="store-content">
<div class="container">
<div class="inner-content">


<div id="product" itemscope="" itemtype="http://schema.org/Product">

<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.elevateZoom.min.js?v=1503003150"></script>
<div class="row">
<div class="col-xs-12">
<ol class="breadcrumb">
<li><a href="<?php echo BASE_URL;?>">Página Principal</a></li>


<li class="active"><?php echo $product_info['name']; ?></li>

</ol>     </div>
</div>
<div class="row">
<div class="col-xs-12">
<div class="row showcase">
<div class="col-xs-6">
<div id="gallery" class="prod-image-thumbs">


    <?php foreach($product_images as $img): ?>

        <a href='javascript: void(0);' data-image='<?php echo BASE_URL; ?>media/products/<?php echo $img['url']; ?>' data-zoom-image='<?php echo BASE_URL; ?>media/products/<?php echo $img['url']; ?>' class="active opt-no" >
            <img src='<?php echo BASE_URL; ?>media/products/<?php echo $img['url']; ?>' style="max-width: 75px;" alt="Birken Western Black" />
        </a>

    <?php endforeach; ?>




</div>
<div class="prod-image text-center">
<img src="<?php echo BASE_URL; ?>media/products/<?php echo $product_images[0]['url']; ?>" alt="Birken Western Black MI8691" title=""   id="zoom" data-zoom-image="<?php echo BASE_URL; ?>media/products/<?php echo $product_images[0]['url']; ?>"/>
</div>
<script>
var zoomConfig = {cursor: "crosshair", easing: true, gallery: 'gallery', galleryActiveClass: 'active'};
$('#zoom').elevateZoom(zoomConfig);
</script>
</div>
<div class="col-xs-6">
<h1 class="prod-title" itemprop="name" style="color: #de058e;">
<?php echo $product_info['name']; ?>
</h1>
<div class="prod-sub clearfix">
<span class="prod-ref pull-left">
Marca:
<span itemprop="sku">
<?php echo $product_info['brand_name']; ?>
</span>
</span>
<span class="prod-secure pull-right">
<i class="glyphicon glyphicon-lock"></i>
Compra Segura
</span>
</div>
<div class="prod-excerpt">
<p></p>
</div>
<div class="prod-subacts">
<div class="row">

<div class="col-xs-6">
<p class="add-to-wishlist">
        <?php if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])):?>
            <a class="btn buy-btn btn-default btn-sm " href="<?php echo BASE_URL;?>wishlist/add/<?php echo $product_info['id'];?>" style="background-color: #de058e;"><i class="glyphicon glyphicon-heart"></i>&nbsp;&nbsp;Adicionar a Lista de Desejo</a>
        <?php else:?>
            <a class="btn buy-btn btn-default btn-sm disabled" href="<?php echo BASE_URL;?>wishlist/add/<?php echo $product_info['id'];?>" style="background-color: #de058e;"><i class="glyphicon glyphicon-heart"></i>&nbsp;&nbsp;Adicionar a Lista de Desejo</a>
        <?php endif;?>
    </p>
<form action="<?php echo BASE_URL; ?>cart/add" class="addwish-btn-form " method="POST" accept-charset="utf-8">
    <div style="display:none">
        <input type="hidden" name="id" value="2838781" />
        <input type="hidden" name="variant_id" value="" />
    </div>
    
</form>

<div class="wish-alert">
    
</div>

<script>
$('.add-to-wishlist').find('a').removeClass('btn-sm btn-default').addClass('btn-primary');
</script>                                   
</div>
</div>
</div>

<form action="<?php echo BASE_URL; ?>cart/add" class="form-cart" method="POST" accept-charset="utf-8">
<div class="prod-variants clearfix">
<ul>        
<li class="prod-variant-li clearfix">
<div class="prod-variant">
<span class="prod-variant-title">
Selecione a opção para
<span class="color">Numeração:</span>
</span>
<ul class="list-inline">
	
	<?php 
	
	if(isset($product_info['type']) and isset($product_info['type']['size'])){
		
		foreach($product_info['type']['size'] as $size){

		?>
				
			<li>
			<div class="sub btn btn-default prod-variant-btn btn-size-id" data-id="<?php echo $size['id_option']?>" quant='<?php echo $size['quant']?>'><?php echo $size['text_size']?></div>
			</li>
		<?php
		
		}
	}
	?>

</ul>
</div>
</li>
</ul>                                                                          </div>

<script>
$(function(){
    function change_pic (option) {

$variation_photo = $('.opt-' + option);
$variation_photo_first = $('.opt-' + option + ':first');
$variation_photo_global = $('.opt-no');
$variation_not = $('.prod-image-thumbs a');

$variation_photo_first.trigger('click');
$variation_not.hide();
$variation_photo.show();
$variation_photo_global.show();


}

$('.prod-variant-btn').click(function(e){
change_pic($(this).data('id'));
});
$('.opt-no').click(function(){
    imagem = $(this).attr('data-image');
  
    $('#carregando').hide();

    $('.zoomContainer').attr('src',imagem);
});
});

</script>
<div class="prod-action">
<div class="row">
    <div class="col-xs-7">
        <div class="price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
            <span class="price">
                <strong class="buy-price">
                <span itemprop="priceCurrency" content="BRL"></span>
                <span class="sale color product_price" itemprop="price" content="<?php echo $product_info['price'];?>" ><?php echo "R$".number_format($product_info['price'], 2); ?></span>
                <span class="sale color variant_price" style="display: none;"><?php echo $product_info['price'];?></span>
                </strong>
                <span class="installments-price">
                Em até <strong class="color">12x</strong>
                de <strong class="color installment-price"><?php echo "R$".number_format(($product_info['price'] / 12), 2, ",", ".");?></strong>
                <span>sem juros </span>
                </span>
            </span>
        </div>
    </div>
<div class="col-xs-5">
<div style="display:none">
    <input type="hidden" name="cartkey" value="">
    <input type="hidden" name="id_product" value="<?php echo $product_info['id'];?>">
    <input type="hidden" name="qt_product" value="1">
    <input type="hidden" name="qt_option_size_id" class="qt_option_size_id" value="">
    <input type="hidden" name="qt_option_size_value" value="">
</div>
<?php if($product_info['stock'] == 0):?>
    <button id="buy-btn" class="btn btn-primary btn-lg buy-btn disabled" type="submit" value="submit" style="background-color: #de058e;">
<i class="glyphicon glyphicon-shopping-cart"></i> Comprar
</button>
<?php else:?>
    <button id="buy-btn" class="btn btn-primary btn-lg buy-btn" type="submit" value="submit" style="background-color: #de058e;">
<i class="glyphicon glyphicon-shopping-cart"></i> Comprar
</button>
<?php endif;?>

<script>
	
	
	$(function(){
		
		$('.form-cart').submit(function(){
			qt_option_size_id = $('input[name=qt_option_size_id]').val();
			qt_option_size_value = $('input[name=qt_option_size_value]').val();
			if(qt_option_size_id == ''){
				alert('selecione uma tamanho');
				return false;
			}
		});
	
		$('.btn-size-id').click(function(){
			$('.btn-size-id').removeClass('active');
			$(this).addClass('active');
			id = $(this).attr('data-id');
			quant = $(this).attr('quant');

			$('input[name=qt_option_size_id]').val(id);
			$('input[name=qt_option_size_value]').val(quant);
		
		});
		
	})
	

</script>

<p class="buy-message hide">Escolha uma opção</p>

</form>
</div>
</div>
    <?php if($product_info['stock'] == 0):?>
        <div class="row">
    <div class="col-xs-12">

        <form action="<?php echo BASE_URL; ?>actions/avise-me" class="remindme-form form-horizontal" method="POST" accept-charset="utf-8">
            <div style="">
                <div class="alert-unavailable alert alert-danger">
                    Produto indisponível
                </div>
            <input type="hidden" name="variant_id" value="<?php echo $product_info['id'];?>" />
            </div>

            <fieldset>
                <label for="email">
                Avise-me quando chegar
                </label>

                <div class="input-group">
                    <input type="text" name="email" class="form-control" placeholder="Email" />
                    <span class="input-group-btn">
                    <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                    </span>
                </div>
            </fieldset>
        </form>
    </div>
</div>
    <?php endif;?>
</div>


<div class="prod-share">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox"></div>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ab71bf4ddfa7a48"></script>
</div>
</div>
</div>

<div class="prod-description">
<ul class="nav nav-tabs">
<li class="active">
<a href="#tab-description" data-toggle="tab">Descrição</a>
</li>

</ul>
<div class="tab-content">
<div class="tab-pane active" id="tab-description">
<p style="margin-bottom: 0px; font-family: &quot;Didact Gothic&quot;; color: rgb(153, 153, 153);">
    <strong>Características</strong><br>
    <?php echo $product_info['description'];?>
</p>
<p class="MsoNormal" style="margin-bottom: 0px; font-family: &quot;Didact Gothic&quot;; color: rgb(153, 153, 153);">Produto com&nbsp;
    <span style="color: rgb(112, 173, 71);">
    FORMA NORMAL</span>: Aconselhamos a compra da sua numeração habitual.
</p>
</div>

<div class="tab-pane active  clearfix" id="tab-description">

    <h2>O que os clientes dizem</h2>
        
        <?php foreach($product_rates as $rate):?>
            <div class="testimonials-listing" style="width: 700px; text-align: justify;">
                <div class="testimonial row" itemprop="review" itemscope itemtype="http://schema.org/Review">
                    <div class="col-md-8">
                        <strong class="testimonial-author" itemprop="author"><?php echo $rate['user_name']; ?></strong> escreveu na data  
                        <strong class="testimonial-date" itemprop="datePublished"><?php echo $rate['date_rated']; ?></strong>:
                        <div class="testimonial-message" itemprop="description"><?php echo $rate['comment'];?></div>
                        
                    </div>
                    <div class="col-md-4" style="margin-top: -30px;">
                        <h3>Estrelas do produto:</h3>
                        <?php for($q=0;$q<intval($rate['points']);$q++): ?>
                <img src="<?php echo BASE_URL; ?>assets/images/star.png" border="0" height="15" />
            <?php endfor; ?>
                    </div>
                </div>
                <hr>
        </div>
        <?php endforeach;?>
    </div>


    <?php if(isset($_SESSION['usersLV'])):?>
        <div class="tab-pane" id="tab-testimonials">

            <h2>Diga-nos o que achou do produto</h2>
            <strong>NOME: </strong><?php echo $user['name']." ".$user['nick'];?><br><br>
            <form method="POST" class="form-horizontal" style="width: 500px;">
                <div class="form-horizontal">
                    <textarea name="depoimento" class="form-control" style="margin-bottom: 20px;" required=""></textarea>
                </div>
                <div class="form-horizontal">
                    <label><strong>Estrelas do produto:</strong></label>
                    <select class="form-control" name="stars" required="">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                </div><br>

                <button class="btn btn-primary pull-right">Comentar</button><br>

            </form>
        </div>

    <?php else:?>
        <div class="alert alert-warning">Faça <a href="<?php echo BASE_URL;?>login" style="color: #c09853;"><strong>login</strong></a> para comentar também!</div>
    <?php endif;?>

    