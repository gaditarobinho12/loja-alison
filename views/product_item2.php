          <div class="product-list-item col-xs-4">
          	
	<div class="prod-wrapper">
			    					    				    					    				    					    				    					    				    					    				    				        				    				    						
		<div class="imgGroup ig-default">
			<a href="<?php echo BASE_URL; ?>product/open/<?php echo $id; ?>">
				<div class="prod-image imgLiquidFill imgLiquid">
					<div class="prod-image-overflow">
					<img src="<?php echo BASE_URL; ?>media/products/<?php echo $images[0]['url']; ?>" alt="<?php echo $name; ?>" style="display: block;" />
					</div>
				</div>
				<div class="prod-image imgLiquidFill imgLiquid prod-hover-image">
					<div class="prod-image-overflow">
					<?php if(!empty($images[1]['url'])):?>
					<img src="<?php echo BASE_URL; ?>media/products/<?php echo $images[1]['url']; ?>" alt="<?php echo $name; ?>" style="display: block;" />
					<?php else:?>
					<img src="<?php echo BASE_URL; ?>media/products/<?php echo $images[0]['url']; ?>" alt="<?php echo $name; ?>" style="display: block;" />
					<?php endif;?>
					</div>
				</div>
			</a>
		</div>

								    			    			
		<div class="colorsOption">
			<ul class="colorsBox">        			        				        	
			</ul>
		</div>
		
		<div class="prod-info">
			<a title="<?php echo $name; ?>" href="<?php echo BASE_URL; ?>product/open/<?php echo $id; ?>"><h3 style="color: #de058e;"><?php echo $name; ?></h3></a>
			<span class="price">
				<p class="buy-price">
					<strong ><?php echo "R$".number_format($price, 2, ',', '.'); ?></strong>
				</p>
				<p class="installments-price">
				<strong>de
					<?php 
						if($price_from != '0') {
							echo 'R$ '.number_format($price_from, 2, ',', '.');
						}
					?>					
					</strong>
				</p>
			</span>

		</div>

		<div class="prod-action">
			<a title="<?php echo $name; ?>" href="<?php echo BASE_URL; ?>product/open/<?php echo $id; ?>">
				<span style="color: #de058e;">
					<i class="glyphicon glyphicon-shopping-cart"></i>
					Ver Produto
				</span>
			</a>
		</div>

		<script>
			$(document).ready(function(){
			$('.product-list-item .prod-action').css('display','none');
			});
		</script>
	</div>
</div>

