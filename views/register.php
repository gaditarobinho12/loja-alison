<section id="store-content">
    		<div class="container">
    		    <div class="inner-content">
        			

<h1 class="page-title">Criar nova conta</h1>
<div class="row">
    <div class="col-lg-12">
                
        
        <form class="form-horizontal" method="post" accept-charset="utf-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-asterisk" style="color: #de058e;"></i>
                    Dados para acesso
                </h3>

            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">
                        Email
                        <sup>*</sup>
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="email" value="<?php 
                            if(isset($_POST['email1']) && !empty($_POST['email1'])){
                                echo $_POST['email1'];
                            }
                        ?>" placeholder="Email" class="form-control" autocomplete="off" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">
                        Senha
                        <sup>*</sup>
                    </label>
                    <div class="col-md-5">
                        <input type="password" name="senha" placeholder="Sua senha" class="form-control" autocomplete="off" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-bookmark" style="color: #de058e;"></i>
                    Dados pessoais
                </h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label for="name" class="col-md-3 control-label">
                        Nome
                        <sup>*</sup>
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="nome" value="" placeholder="Nome" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-3 control-label">
                        Sobrenome
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="sobrenome" value="" placeholder="Sobrenome" class="form-control">
                    </div>
                </div>

                <div class="brasil-only">
                    <div class="form-group">
                        <label for="phone" class="col-md-3 control-label">
                            Telefone
                            <sup>*</sup>
                        </label>
                        <div class="col-md-5">
                            <input type="text" name="telefone" value="" id="phone_id" placeholder="(21) 12345-1234" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cpf" class="col-md-3 control-label">
                            CPF
                            <sup>*</sup>
                        </label>
                        <div class="col-md-5">
                            <input type="text" name="cpf" value="" placeholder="000.000.000-00" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>

                <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-home" style="color: #de058e;"></i>
                    Endereço de Entrega
                </h3>
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <div class="brasil-only">

                        <label class="col-md-3 control-label">
                            CEP
                        </label>

                        <div class="input-group col-md-5">
                            <input type="text" name="cep" value="" id="cep" placeholder="CEP" class="address form-control">
                            
                        </div>

                        <span class="input-group-btn input-group-search-address col-md-4" style="margin-left: 7em;">
                            <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="btn btn-default link-consult-cep pull-right" target="_blank">
                                Consultar CEP
                            </a>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="zone_id" class="col-md-3 control-label">
                        Estado
                    </label>
                    <div class="col-md-5">
                        <select name="estado" class="form-control" id="uf">
                            <option value="">Selecionar</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="a_city" class="col-md-3 control-label">
                        Cidade
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="cidade" value="" class="form-control" placeholder="Cidade">
                    </div>
                </div>

                <div class="form-group">
                    <label for="a_district" class="col-md-3 control-label">
                        Bairro
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="bairro" value="" class="form-control" placeholder="Bairro">
                    </div>
                </div>

                <div class="form-group">
                    <label for="a_address1" class="col-md-3 control-label">
                        Rua
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="rua" value="" class="form-control" placeholder="Endereço">
                    </div>
                </div>

                <div class="form-group">
                    <label for="a_number" class="col-md-3 control-label">
                        Número
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="numero" value="" class="form-control" placeholder="Número">
                    </div>
                </div>

                <div class="form-group">
                    <label for="a_address2" class="col-md-3 control-label">
                        Complemento
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="complemento" value="" class="form-control" placeholder="Complemento">
                    </div>
                </div>

                
<input type="hidden" name="address_id" value="">

            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <label>
                        <input type="checkbox" name="email_subscribe" value="1" checked="checked">
                        Deseja receber nossa newsletter?
                    </label>
                    <br>
                    <small>Não compartilhamos seu e-mail com terceiros.</small>
                </div>
                <div class="pull-right">
                    * Campos obrigatórios
                </div>
            </div>
        </div>
        <div class="pull-right">
            <a href="<?php echo BASE_URL;?>login" class="btn btn-default">
                <i class="glyphicon glyphicon-chevronl-eft"></i>
                Voltar
            </a>
            <input type="submit" value="Cadastre-se" class="btn btn-primary btn-bck" style="background-color: #de058e;">
        </div>

        </form>
    </div>
</div>



		        
    		    </div>
    		</div>
    	</section>

        <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>