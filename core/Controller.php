<?php
class controller {

	protected $db;

	public function __construct() {
		global $config;
	}
	
	public function loadView($viewName, $viewData = array()) {
		extract($viewData);
		
		include 'views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()) {
		$view = 'views/templates/index.php';
	
		if(file_exists(__DIR__."/../views/template.{$viewName}.php")){

		//	$view = "views/template.{$viewName}.php";

		
		}
		include $view;
	}

	public function loadTemplateUsers($viewName, $viewData = array()) {
		include 'views/templateUsers.php';
	}

	public function loadTemplateCloud($viewName, $viewData = array()) {
		include 'views/templateCloud.php';
	}

	public function loadViewInTemplate($viewName, $viewData) {
		extract($viewData);
		include 'views/'.$viewName.'.php';
	}

}