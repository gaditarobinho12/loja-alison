<?php
class Cart extends Model {
	private $desconto;

	public function getList() {
		$products = new Products();

		$array = array();
		$cart = array();

		if(isset($_SESSION['cart'])) {
			$cart = $_SESSION['cart'];
		}

		foreach($cart as $list => $data) {
			
		
			if(isset($data) and is_array($data))
			foreach($data as $_data){
				$id = $_data['id_product'];
				$qt =  $_data['quant'];
				$info = $products->getInfo($id);
			
			$size = $products->getOptionTypesSize($id,$_data['option_id']);
			$size = $size['size'];
			$error = false;

			if(count($size) and isset($size) and $size['quant']  < $qt  ){
				$error = "Somente temos ".$size['quant']." em estoque";
					$size = [
					'text_size'=>'',
					'quant'=>0
				];
				
			}
			$array[] = array(
				'id' => $id,
				'qt' => $qt,
				'price' => $info['price'],
				'name' => $info['name'],
				'image' => $info['image'],
				'weight' => $info['weight'],
				'width' => $info['width'],
				'height' => $info['height'],
				'length' => $info['lenght'],
				'diameter' => $info['diameter'],
				'error' =>$error,
				'size'=>$size 
			);

			}
			
		}
	

		return $array;
	}

	public function verificaCode($code){
		$array = array();
		$sql = "SELECT * FROM coupons WHERE name = :name";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $code);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
			$this->desconto = $array['coupon_value'];
			return $array;
		} else{
			return false;
		}
	}

	public function getSubtotal() {
		$list = $this->getList();

		$subtotal = 0;

		foreach($list as $item) {
			$subtotal += (floatval($item['price']) * intval($item['qt']));
		}

		return $subtotal;
	}

	public function shippingCalculate($cepDestination) {
		$array = array(
			'price' => 0,
			'date' => '',
		);

		global $config;


		$list = $this->getList();

		$nVlPeso = 0;
		$nVlComprimento = 0;
		$nVlAltura = 0;
		$nVlLargura = 0;
		$nVlDiametro = 0;
		$nVlValorDeclarado = 0;

		foreach($list as $item) {
			$nVlPeso += floatval($item['weight']);
			$nVlComprimento += floatval($item['length']);
			$nVlAltura += floatval($item['height']);
			$nVlLargura += floatval($item['width']);
			$nVlDiametro += floatval($item['diameter']);
			$nVlValorDeclarado += floatval($item['price'] * $item['qt']);
		}

		$soma = $nVlComprimento + $nVlAltura + $nVlLargura;
		if($soma > 200) {
			$nVlComprimento = 66;
			$nVlAltura = 66;
			$nVlLargura = 66;
		}

		if($nVlDiametro > 90) {
			$nVlDiametro = 90;
		}

		if($nVlPeso > 40) {
			$nVlPeso = 40;
		}

		$data = array(
			'nCdServico' => '40010',
			'sCepOrigem' => $config['cep_origin'],
			'sCepDestino' => $cepDestination,
			'nVlPeso' => $nVlPeso,
			'nCdFormato' => '1',
			'nVlComprimento' => $nVlComprimento,
			'nVlAltura' => $nVlAltura,
			'nVlLargura' => $nVlLargura,
			'nVlDiametro' => $nVlDiametro,
			'sCdMaoPropria' => 'N',
			'nVlValorDeclarado' => $nVlValorDeclarado,
			'sCdAvisoRecebimento' => 'N',
			'StrRetorno' => 'xml'
		);

		$url = 'http://ws.correios.com.br/calculador/CalcPrecoprazo.aspx';
		$data = http_build_query($data);

		$ch = curl_init($url.'?'.$data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$r = curl_exec($ch);
		$r = simplexml_load_string($r);

		$array['price'] = current($r->cServico->Valor);
		$array['date'] = current($r->cServico->PrazoEntrega);

		return $array;
	}
}