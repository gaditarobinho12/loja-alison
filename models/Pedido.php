<?php 
class Pedido extends Model{

	public function getItensPedido($id){
		$array = array();

		$sql = "SELECT * FROM purchases_products WHERE id_purchase = :id_purchase";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id_purchase", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getNome($id){
		$array = array();
		$sql = "SELECT name FROM products WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function getDataPurchase($id){
		$array = array();

		$sql = "SELECT * FROM purchases WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function updateSize($size, $idItem){
		$sql = "UPDATE purchases_products SET size = :size WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":size", $size);
		$sql->bindValue(":id", $idItem);
		$sql->execute();
	}
}