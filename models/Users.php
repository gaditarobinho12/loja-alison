<?php
class Users extends Model {

	public function emailExists($email) {

		$sql = "SELECT * FROM users WHERE email = :email";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->execute();

		if($sql->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function validate($email, $pass) {
		$uid = '';

		$sql = "SELECT * FROM users WHERE email = :email AND password = :pass";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->bindValue(":pass", md5($pass));
		$sql->execute();

		if($sql->rowCount() > 0) {
			$sql = $sql->fetch();
			$uid = $sql['id'];
		} else{
			return false;
		}

		return $uid;
	}

	public function verificaSeExiste($email){
		$sql = "SELECT * FROM users WHERE email = :email";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->execute();

		if($sql->rowCount() > 0){
			return false;
		} else{
			return true;
		}
	}

	public function createUser($email, $senha, $nome, $sobrenome, $telefone, $cpf, $cep, $estado, $cidade, $bairro, $rua, $numero, $complemento) {

		$sql = "INSERT INTO users (email, password, telefone, cep, endereco, numero, complemento, bairro, cidade, estado, name, nick, cpf) VALUES (:email, :pass, :telefone, :cep, :endereco, :numero, :complemento, :bairro, :cidade, :estado, :nome, :nick, :cpf)";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->bindValue(":pass", md5($senha));
		$sql->bindValue(":telefone", $telefone);
		$sql->bindValue(":cep", $cep);
		$sql->bindValue(":endereco", $rua);
		$sql->bindValue(":numero", $numero);
		$sql->bindValue(":complemento", $complemento);
		$sql->bindValue(":bairro", $bairro);
		$sql->bindValue(":cidade", $cidade);
		$sql->bindValue(":estado", $estado);
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":nick", $sobrenome);
		$sql->bindValue(":cpf", $cpf);
		$sql->execute();

		$_SESSION['usersLV'] = $this->db->lastInsertId();

		return $this->db->lastInsertId();

	}

	public function logar($email, $senha){
		$erro = array(
			'erro' => 'Erro: E-mail e/ou senha incorreta'
		);
		$sql = "SELECT id FROM users WHERE email = :email AND password = :pass";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->bindValue(":pass", md5($senha));
		$sql->execute();

		if($sql->rowCount() > 0){
			$sql = $sql->fetch();
			$_SESSION['usersLV'] = $sql['id'];

			header("Location: ".BASE_URL."users");
		} else{
			return $erro['erro'];
		}
	}

	public function getDados(){
		$array = array();

		if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])){
			$sql = "SELECT * FROM users WHERE id = :id";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":id", $_SESSION['usersLV']);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetch();
			}

			return $array;
		}
	}

	public function getPedidos(){
		if(!isset($_SESSION['usersLV']) && empty($_SESSION['usersLV'])){
			$_SESSION['usersLV'] = 0;
		}
		$array = array();
		$sql = "SELECT * FROM purchases WHERE id_user = :id_user";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id_user", $_SESSION['usersLV']);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function update($senha, $name, $nick, $telefone){

		$sql = "UPDATE users SET password = :pass, name = :name, nick = :nick, telefone = :telefone WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":pass", md5($senha));
		$sql->bindValue(":name", $name);
		$sql->bindValue(":nick", $nick);
		$sql->bindValue(":telefone", $telefone);
		$sql->bindValue(":id", $_SESSION['usersLV']);
		$sql->execute();
	}

	public function updateAddress($nome, $sobrenome, $rua, $numero, $complemento, $cidade, $bairro, $estado, $telefone){
		$sql = "UPDATE users SET name = :name, nick = :nick, endereco = :endereco, numero = :numero, complemento = :complemento, cidade = :cidade, bairro = :bairro, estado = :estado, telefone = :telefone WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $nome);
		$sql->bindValue(":nick", $sobrenome);
		$sql->bindValue(":endereco", $rua);
		$sql->bindValue(":numero", $numero);
		$sql->bindValue(":complemento", $complemento);
		$sql->bindValue(":cidade", $cidade);
		$sql->bindValue(":bairro", $bairro);
		$sql->bindValue(":estado", $estado);
		$sql->bindValue(":telefone", $telefone);
		$sql->bindValue(":id", $_SESSION['usersLV']);
		$sql->execute();
	}
}