<?php 
class WishList extends Model{
	public function getList(){
		$array = array();
		$sql = "SELECT * FROM wishlist WHERE id_user = :id_user";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id_user", $_SESSION['usersLV']);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchALl();
		}

		return $array;
	}

	public function addWishlist($id, $id_user){
		$sql = "INSERT INTO wishlist SET id_user = :id_user, id_product = :id_product";
    	$sql = $this->db->prepare($sql);
    	$sql->bindValue(":id_user", $id_user);
    	$sql->bindValue(":id_product", $id);
    	$sql->execute();

    	header("Location: ".BASE_URL."product/open/".$id);
	}

	public function delWishList($id){
		$sql = "DELETE FROM wishlist WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		header("Location: ".BASE_URL."users/#mywishlist");
	}
}