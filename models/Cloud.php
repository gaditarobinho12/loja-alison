<?php
class Cloud extends Model{
	public function logar($login, $pass){
		$sql = "SELECT * FROM adm WHERE login = :login AND password = :pass";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":login", $login);
		$sql->bindValue(":pass", md5($pass));
		$sql->execute();

		if($sql->rowCount() > 0){
			$sql = $sql->fetch();

			$_SESSION['cloudLV'] = $sql['id'];
			header("Location: ".BASE_URL."cloud");
		}
	}

	public function getNewPurchases(){
		$array = array();

		$sql = "SELECT * FROM purchases WHERE status = 1 ORDER BY id ASC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;

	}

	public function getDadosCliente($id){
		$array = array();

		$sql = "SELECT * FROM users WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function getDadosPedido($id){
		$array = array();

		$sql = "SELECT * FROM purchases WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function getOtherOrdens(){
		$array = array();

		$sql = "SELECT * FROM purchases WHERE status != 1 ORDER BY id ASC";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getItensPedidos($id){
		$array = array();

		$sql = "SELECT * FROM purchases_products WHERE id_purchase = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getDadosProduct($id){
		$array = array();

		$sql = "SELECT * FROM products WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
			$array['fotos'] = array();

			$sql = "SELECT id,url FROM products_images WHERE id_product = :id_product";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":id_product", $id);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array['fotos'] = $sql->fetchAll();
			}
		}

		return $array;
	}

	public function insertObjeto($objeto, $id){
		$sql = "UPDATE purchases SET objeto = :objeto, status = 2 WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":objeto", $objeto);
		$sql->bindValue(":id", $id);
		$sql->execute();
	}

	public function getCats(){
		$array = array();

		$sql = "SELECT * FROM categories";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getInfoCat($id){
		$array = array();

		$sql = "SELECT * FROM categories WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function addCat($nome_cat, $cat_pai){
		$sql = "INSERT INTO categories SET name = :nome, sub = :sub";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":nome", $nome_cat);
		$sql->bindValue(":sub", $cat_pai);
		$sql->execute();
	}

	public function editarCat($nome_cat, $cat_pai, $id){
		$sql = "UPDATE categories SET name = :name, sub = :sub WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $nome_cat);
		$sql->bindValue(":sub", $cat_pai);
		$sql->bindValue(":id", $id);
		$sql->execute();
		return true;
	}

	public function excluirCat($id){
		$sql = "DELETE FROM categories WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();
	}

	public function getMarcas(){
		$array = array();
		$sql = "SELECT * FROM brands";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getSizes($id){
		$array = array();
		$sql = "SELECT * FROM `products_options`
		LEFT JOIN products_options_type ON products_options.id = products_options_type.id_option
		WHERE products_options.type = 'size' and id_product = $id";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getDadosMarca($id){
		$array = array();
		$sql = "SELECT * FROM brands WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function addMarca($marca){
		$sql = "INSERT INTO brands SET name = :name";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $marca);
		$sql->execute();
		return true;
	}

	public function editarMarca($marca, $id){
		$sql = "UPDATE brands SET name = :name WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $marca);
		$sql->bindValue(":id", $id);
		$sql->execute();
		return true;
	}

	public function excluirMarca($id){
		$sql = "DELETE FROM brands WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();
	}

	public function getComentarios(){
		$array = array();

		$sql = "SELECT * FROM rates";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function getInfoComents($id){
		$array = array();
		$sql = "SELECT * FROM rates WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetch();
		}

		return $array;
	}

	public function updateComent($comentario, $id){
		$sql = "UPDATE rates SET comment = :comentario WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":comentario", $comentario);
		$sql->bindValue(":id", $id);
		$sql->execute();
	}

	public function deleteComent($id){
		$sql = "DELETE FROM rates WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();
	}

	//Produtos

	public function getProducts(){
		$array = array();

        $sql = "SELECT * FROM products";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0){
            $array = $sql->fetchAll();
        }

        return $array;
	}

	public function updateProduct($nome, $descricao, $preco, $preco_anterior, $categoria, $marca, $estoque, $peso, $largura, $altura, $comprimento, $fotos, $id){
		
		$sql = "UPDATE products SET name = :name, description = :description, price = :price, price_from = :price_from, id_category = :id_category, id_brand = :id_brand, stock = :stock, weight = :weight, width = :width, height = :height, lenght = :lenght WHERE id = :id";

		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $nome);
		$sql->bindValue(":description", $descricao);
		$sql->bindValue(":price", $preco);
		$sql->bindValue(":price_from", $preco_anterior);
		$sql->bindValue(":id_category", $categoria);
		$sql->bindValue(":id_brand", $marca);
		$sql->bindValue(":stock", $estoque);
		$sql->bindValue(":weight", $peso);
		$sql->bindValue(":width", $largura);
		$sql->bindValue(":height", $altura);
		$sql->bindValue(":lenght", $comprimento);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if(count($fotos) > 0){
			for($q=0;$q<count($fotos['type']);$q++){ 
				$tipo = $fotos['type'][$q];

				if(in_array($tipo, array('image/jpeg', 'image/png'))){
					$tmpname = md5(time().rand(0, 9999)).'.jpg';
					move_uploaded_file($fotos['tmp_name'][$q], __DIR__."/../media/products/".$tmpname);

					//$dir_upload = __DIR__.'/../media/';
				}
			}
		}

		return true;

	}

	function removeSize($id_product){


		$sql = "SELECT * FROM products_options WHERE id_product = :id_product";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id_product", $id_product);
		$sql->execute();

		if($sql->rowCount() > 0){
			$data = $sql->fetchAll();
			
			foreach($data as $d){
				$id = $d['id'];
				$sql = "DELETE FROM `products_options_type` WHERE `products_options_type`.`id_option` = $id";

				$sql = $this->db->query($sql);
		
			}
			
		}

		$sql = "DELETE FROM `products_options` WHERE `products_options`.`id_product` = $id_product";

		$sql = $this->db->query($sql);

	}
	function addSize($data){
		$sql = "SELECT * FROM options WHERE name = 'tamanho'";
		$sql = $this->db->prepare($sql);
		$sql->execute();

		if($sql->rowCount() > 0){
			$data_opt = $sql->fetch();
		

			$id_option = $data_opt['id'];

		}
		if(count($data))
		$this->removeSize($data[0]['id_product']);

		foreach($data as $d){
			if(!empty($d['size']) and !empty($d['quant'])){
				$id_product = $d['id_product'];
				$p_value = $d['size'];

				$sql = "INSERT INTO products_options SET id_product = :id_product, id_option = :id_option,
				p_value = :p_value, type = 'size'";

				$sql = $this->db->prepare($sql);
				$sql->bindValue(":id_product", $id_product);
				$sql->bindValue(":id_option", $id_option);
				$sql->bindValue(":p_value", $p_value);
				$sql->execute();
		
				$pid_opt = $this->db->lastInsertId();
		

				$value = $d['quant'];
				$id_product = $d['id_product'];


				$sql = "INSERT INTO products_options_type SET id_option = :id_option, value = :value, type = 'size'";

				$sql = $this->db->prepare($sql);
				$sql->bindValue(":id_option", $pid_opt);
				$sql->bindValue(":value", $value);
				$sql->execute();
			}
		}
	}
	public function addProduct($nome, $descricao, $preco, $preco_anterior, $categoria, $marca, $peso, $largura, $altura, $comprimento, $fotos){
		$sql = "INSERT INTO products SET name = :name, description = :description, price = :price, price_from = :price_from, id_category = :id_category, id_brand = :id_brand, weight = :weight, width = :width, height = :height, lenght = :lenght";

		$sql = $this->db->prepare($sql);
		$sql->bindValue(":name", $nome);
		$sql->bindValue(":description", $descricao);
		$sql->bindValue(":price", $preco);
		$sql->bindValue(":price_from", $preco_anterior);
		$sql->bindValue(":id_category", $categoria);
		$sql->bindValue(":id_brand", $marca);
		$sql->bindValue(":weight", $peso);
		$sql->bindValue(":width", $largura);
		$sql->bindValue(":height", $altura);
		$sql->bindValue(":lenght", $comprimento);
		$sql->execute();

		$pid = $this->db->lastInsertId();

		if(count($fotos) > 0){
			for($q=0;$q<count($fotos['type']);$q++){ 
				$tipo = $fotos['type'][$q];

				if(in_array($tipo, array('image/jpeg', 'image/png'))){
					$tmpname = md5(time().rand(0, 9999)).'.jpg';
					move_uploaded_file($fotos['tmp_name'][$q], __DIR__."/../media/products/".$tmpname);

					list($width_orig, $height_orig) = getimagesize( __DIR__."/../media/products/".$tmpname);

					$ratio = $width_orig/$height_orig;

					$width = 934;
					$height = 1400;

					$img = imagecreatetruecolor($width, $height);

					if($tipo = 'image/jpeg'){
						$orig = imagecreatefromjpeg( __DIR__."/../media/products/".$tmpname);
					} elseif ($tipo = 'image/png') {
						$orig = imagecreatefrompng( __DIR__."/../media/products/".$tmpname);
					}

					imagecopyresampled($img, $orig, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

					imagejpeg($img,  __DIR__."/../media/products/".$tmpname, 80);

					$sql = "INSERT INTO products_images SET id_product = :id_product, url = :url";
					$sql = $this->db->prepare($sql);
					$sql->bindValue(":id_product", $pid);
					$sql->bindValue(":url", $tmpname);
					$sql->execute();
				}
			}
		}

		return $pid;

	}

	public function deleteProduct($id){
		$sql = "SELECT * FROM products_images WHERE id_product = :id_product";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id_product", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$sql = $sql->fetchAll();

			foreach($sql as $foto){
				unlink(__DIR__."/../media/products/".$foto['url']);
			}
			
		}

		$sql = "DELETE FROM products_images WHERE id_product = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		$sql = "DELETE FROM products WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		header("Location: ".BASE_URL."cloud/produtos");
	}

	public function deleteFoto($id){
		$sql = "SELECT url,id_product FROM products_images WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if($sql->rowCount() > 0){
			$sql = $sql->fetch();

			unlink(__DIR__."/../media/products/".$sql['url']);

			$sql = "DELETE FROM products_images WHERE id = :id";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":id", $id);
			$sql->execute();
		}

		header("Location: ".BASE_URL."cloud/editarProduto/".$id_product);
	}
}