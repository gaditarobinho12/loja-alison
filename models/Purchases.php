<?php
class Purchases extends Model {

	public function createPurchase($uid, $total, $payment_type) {
		date_default_timezone_set('America/Sao_Paulo');
		$sql = "INSERT INTO purchases (id_user, total_amount, payment_type, payment_status, data) VALUES (:uid, :total, :type, 1, :data)";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":uid", $uid);
		$sql->bindValue(":total", $total);
		$sql->bindValue(":type", $payment_type);
		$sql->bindValue(":data", date("d/m/Y"));
		$sql->execute();

		return $this->db->lastInsertId();

	}

	public function addItem($id, $id_product, $qt, $price, $size) {

		$sql = "INSERT INTO purchases_products (id_purchase, id_product, quantity, product_price, size) VALUES (:id, :idp, :qt, :price, :size)";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->bindValue(":idp", $id_product);
		$sql->bindValue(":qt", $qt);
		$sql->bindValue(":price", $price);
		$sql->bindValue(":size", $size);
		$sql->execute();

	}

	public function setPaid($id) {

		$sql = "UPDATE purchases SET payment_status = :status WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":status", '2');
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function setCancelled($id) {

		$sql = "UPDATE purchases SET payment_status = :status WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":status", '3');
		$sql->bindValue(":id", $id);
		$sql->execute();

	}

	public function updateBilletUrl($id, $link){
		$sql = "UPDATE purchases SET billet_link = :link WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->bindValue(":link", $link);
		$sql->execute();
	}
}