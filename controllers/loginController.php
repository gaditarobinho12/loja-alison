<?php
class loginController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $f = new Filters();
        $u = new Users();

        $dados = $store->getTemplateData();

        $filters = array();
        if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
            $filters = $_GET['filter'];
        }

        $dados['categoriesMenu'] = $categories->getCatFromMenu();

        $dados['categoriesMenu'] = $categories->getCatFromMenu();

        $dados['categoriesMenu'] = $categories->getCatFromMenu();

        $dados['filters'] = $f->getFilters($filters);
        $dados['filters_selected'] = $filters;

        $dados['searchTerm'] = '';
        $dados['category'] = '';

        //$dados['sidebar'] = true;

        if(isset($_POST['email']) && !empty($_POST['email'])){
            $email = addslashes($_POST['email']);
            $senha = $_POST['senha'];

            $login = $u->logar($email, $senha);

            $dados['erro'] = $login;
        }

        $dados['user'] = $u->getDados();

        $this->loadTemplate('login', $dados);
    }
}