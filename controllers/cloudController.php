<?php
class cloudController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }
//Produtos

    public function index() {
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();
            $dados['pedidos'] = $c->getNewPurchases();

            $this->loadTemplateCloud('cloud', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function open($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            if(isset($_POST['objeto']) && !empty($_POST['objeto'])){
                $objeto = addslashes($_POST['objeto']);

                $insere = $c->insertObjeto($objeto, $id);
            }

            $dados = $store->getTemplateData();
            $dados['dataPedidos'] = $c->getDadosPedido($id);

            $dados['itensPedidos'] = $c->getItensPedidos($id);
            $dados['cliente'] = $c->getDadosCliente($dados['dataPedidos']['id_user']);

            $this->loadTemplateCloud('purchase', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function visualisados(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();
            $dados['visualisados'] = $c->getOtherOrdens();

            $this->loadTemplateCloud('visualisados', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

//Categorias

    public function categorias(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();
            
            $dados['cats'] = $c->getCats();

            $this->loadTemplateCloud('categoriasC', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function editarCat($id){

       if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            if(isset($_POST['nome_cat']) && !empty($_POST['nome_cat'])){
                $nome_cat = addslashes($_POST['nome_cat']);
                $cat_pai = addslashes($_POST['cat_pai']);

                $updateC = $c->editarCat($nome_cat, $cat_pai, $id);
            }
            
            $dados['cats'] = $c->getCats();
            $dados['catInfo'] = $c->getInfoCat($id);

            $this->loadTemplateCloud('editarCat', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function addCat(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            if(isset($_POST['nome_cat']) && !empty($_POST['nome_cat'])){
                $nome_cat = addslashes($_POST['nome_cat']);
                if(!empty($_POST['cat_pai'])){
                    $cat_pai = addslashes($_POST['cat_pai']);
                } else{
                    $cat_pai = NULL;
                }

                $updateC = $c->addCat($nome_cat, $cat_pai);
            }
            
            $dados['cats'] = $c->getCats();

            $this->loadTemplateCloud('addCat', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function excluirCat($id){
        $c = new Cloud();
        $excluirCat = $c->excluirCat($id);
        header("Location: ".BASE_URL."cloud/categorias");
    }

//Marcas
    public function marcas(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            $dados['marcas'] = $c->getMarcas();

            $this->loadTemplateCloud('marcas', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function editarMarca($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            $dados['marcas'] = $c->getMarcas();
            

            if(isset($_POST['nome_marca'])){
                $marca = addslashes($_POST['nome_marca']);

                $updateM = $c->editarMarca($marca, $id);
            }

            $dados['dadosMarca'] = $c->getDadosMarca($id);

            $this->loadTemplateCloud('editarMarcas', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function addMarca(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $products = new Products();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            $dados['marcas'] = $c->getMarcas();
            

            if(isset($_POST['nome_marca'])){
                $marca = addslashes($_POST['nome_marca']);

                $addM = $c->addMarca($marca);
            }

            $this->loadTemplateCloud('adicionarMarcas', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function excluirMarca($id){
        $c = new Cloud();
        $excluirMarca = $c->excluirMarca($id);
        header("Location: ".BASE_URL."cloud/marcas");
    }

//Comentarios
    public function comentarios(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            $dados['comentarios'] = $c->getcomentarios();
            
            $this->loadTemplateCloud('comentarios', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function editarComentario($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            if(isset($_POST['comentario']) && !empty($_POST['comentario'])){
                $comentario = addslashes($_POST['comentario']);

                $editarComents = $c->updateComent($comentario, $id);
            }
            
            $dados['infoComents'] = $c->getInfoComents($id);
            
            $this->loadTemplateCloud('editarComentarios', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function excluirComentario($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();

            $excluirComentario = $c->deleteComent($id);
            header("Location: ".BASE_URL."cloud/comentarios");

        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function produtos(){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();

            $dados = $store->getTemplateData();

            $dados['products'] = $c->getProducts();
            
            $this->loadTemplateCloud('produtos', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function editarProduto($id){
      

        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();
            $cats = new Categories();

            $dados = $store->getTemplateData();


            if(isset($_POST['nome']) && !empty($_POST['nome'])){
                $nome = addslashes($_POST['nome']);
                $descricao = addslashes($_POST['descricao']);
                $preco = addslashes($_POST['preco']);
                $preco_anterior = addslashes($_POST['preco_anterior']);
                $categoria = addslashes($_POST['categoria']);
                $marca = addslashes($_POST['marca']);
                $estoque = addslashes($_POST['estoque']);
                $peso = addslashes($_POST['peso']);
                $largura = addslashes($_POST['largura']);
                $altura = addslashes($_POST['altura']);
                $comprimento = addslashes($_POST['comprimento']);

                if(isset($_FILES['fotos']) && !empty($_FILES['fotos'])){
                    $fotos = $_FILES['fotos'];
                }else{
                    $fotos = array();
                }
                
                $data_size = [];
                $i=0;
                foreach($_POST['size'] as $size){
                    $quant = $_POST['size_quant'][$i];
                    $data_size[] = [
                        'id_product' => $id,
                        'quant'=>$quant,
                        'size'=>$size
                    ];
                $i++;
                }
                $c->addSize($data_size);

                if($c->updateProduct($nome, $descricao, $preco, $preco_anterior, $categoria, $marca, $estoque, $peso, $largura, $altura, $comprimento, $fotos, $id)){
                    $dados['avisosS'] = "O produto foi salvo!";
                } else{
                    $dados['avisosD'] = "Erro ao salvar atualização no produto.";
                }
            }

            $dados['productInfo'] = $c->getDadosProduct($id);
            $dados['cats'] = $c->getCats(); 
            $dados['marcas'] = $c->getMarcas();
            
            $dados['size'] = $c->getSizes($id);
           

            $this->loadTemplateCloud('editarProduto', $dados);
        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function addProduto(){
        
        
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $store = new Store();
            $c = new Cloud();
            $cats = new Categories();

            $dados = $store->getTemplateData();


            if(isset($_POST['nome']) && !empty($_POST['nome'])){
                $nome = addslashes($_POST['nome']);
                $descricao = addslashes($_POST['descricao']);
                $preco = addslashes($_POST['preco']);
                $preco_anterior = addslashes($_POST['preco_anterior']);
                $categoria = addslashes($_POST['categoria']);
                $marca = addslashes($_POST['marca']);
                
                $peso = addslashes($_POST['peso']);
                $largura = addslashes($_POST['largura']);
                $altura = addslashes($_POST['altura']);
                $comprimento = addslashes($_POST['comprimento']);

                if(isset($_FILES['fotos']) && !empty($_FILES['fotos'])){
                    $fotos = $_FILES['fotos'];
                }else{
                    $fotos = array();
                }
                $id_product = $c->addProduct($nome, $descricao, $preco, $preco_anterior, $categoria, $marca, $peso, $largura, $altura, $comprimento, $fotos);
                if($id_product){

                    $data_size = [];
                    $i=0;
                    foreach($_POST['size'] as $size){
                        $quant = $_POST['size_quant'][$i];
                        $data_size[] = [
                            'id_product' => $id_product,
                            'quant'=>$quant,
                            'size'=>$size
                        ];
                    $i++;
                    }
                    $c->addSize($data_size);

                    $dados['avisosS'] = "O produto foi salvo!";
                } else{
                    $dados['avisosD'] = "Erro ao salvar atualização no produto.";
                }
            }

            //$dados['productInfo'] = $c->getDadosProduct($id);
            $dados['cats'] = $c->getCats(); 
            $dados['marcas'] = $c->getMarcas();
            
           
           
            $this->loadTemplateCloud('addProduto', $dados);

        } else{
            header("Location: ".BASE_URL."cloudLogin");
        }
    }

    public function excluirProduto($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $c = new Cloud();

            $excluirProduto = $c->deleteProduct($id);
        }
    }

    public function excluirFoto($id){
        if(isset($_SESSION['cloudLV']) && !empty($_SESSION['cloudLV'])){
            $c = new Cloud();

            $excluirFoto = $c->deleteFoto($id);
        }
    }

//Açoes
    public function sair(){
        unset($_SESSION['cloudLV']);

        header("Location: ".BASE_URL."cloudLogin");
    }
}