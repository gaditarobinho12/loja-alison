<?php
class cloudLoginController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $c = new Cloud();

        $dados = array();
        if(isset($_POST['user']) && !empty($_POST['user'])){
            $user = addslashes($_POST['user']);
            $senha = $_POST['senha'];

            $login = $c->logar($user, $senha);

            $dados['erro'] = $login;
        }

        $this->loadTemplateCloud('cloudLogin', $dados);
    }
}