<?php
class pedidoController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function open($id){
        if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])){
            $u = new Users();
            $p = new Pedido();
            $store = new Store();
            $products = new Products();
            $categories = new Categories();
            $f = new Filters();

            
            $dados = $store->getTemplateData();
	        $dados['user'] = $u->getDados();

            $dados['categoriesMenu'] = $categories->getCatFromMenu();

            
	        $dados['itensP'] = $p->getItensPedido($id);
	        $dados['resumo'] = $p->getDataPurchase($id);
            $dados['idP'] = $id;

	        $this->loadTemplate('pedido', $dados);
        } else{
            header("Location: ".BASE_URL);
        }
    }
}