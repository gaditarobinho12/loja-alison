<?php
class confController extends Controller {

    private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])){
            $store = new Store();
            $products = new Products();
            $categories = new Categories();
            $f = new Filters();
            $u = new Users();

            $dados = $store->getTemplateData();

            $filters = array();
            if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
                $filters = $_GET['filter'];
            }

            $dados['filters'] = $f->getFilters($filters);
            $dados['filters_selected'] = $filters;

            $dados['searchTerm'] = '';
            $dados['category'] = '';
            $dados['pedidos'] = $u->getPedidos();
            

            //$dados['sidebar'] = true;

            if(isset($_POST['cep']) && !empty($_POST['cep'])){
                $cep = addslashes($_POST['cep']);
                $endereco = addslashes($_POST['endereco']);
                $numero = addslashes($_POST['numero']);
                $complemento = addslashes($_POST['complemento']);
                $bairro = addslashes($_POST['bairro']);
                $cidade = addslashes($_POST['cidade']);
                $estado = addslashes($_POST['estado']);
                $telefone = addslashes($_POST['telefone']);

                $atualizar = $u->update($cep, $endereco, $numero, $complemento, $bairro, $cidade, $estado, $telefone);
            }

            $dados['user'] = $u->getDados();

            $this->loadTemplateUsers('conf', $dados);
        } else{
            header("Location: ".BASE_URL);
        }
    }
}