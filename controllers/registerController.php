<?php
class registerController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $f = new Filters();
        $u = new Users();

        $dados = $store->getTemplateData();

        $filters = array();
        if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
            $filters = $_GET['filter'];
        }

        $dados['categoriesMenu'] = $categories->getCatFromMenu();

        $dados['filters'] = $f->getFilters($filters);
        $dados['filters_selected'] = $filters;

        $dados['searchTerm'] = '';
        $dados['category'] = '';
        $dados['pedidos'] = $u->getPedidos();
        $dados['user'] = $u->getDados();

        //$dados['sidebar'] = true;

        if(isset($_POST['email']) && !empty($_POST['email'])){
            $email = addslashes($_POST['email']);
            $senha = $_POST['senha'];

            $nome = addslashes($_POST['nome']);
            $sobrenome = addslashes($_POST['sobrenome']);
            $telefone = addslashes($_POST['telefone']);
            $cpf = addslashes($_POST['cpf']);

            $cep = addslashes($_POST['cep']);
            $estado = addslashes($_POST['estado']);
            $cidade = addslashes($_POST['cidade']);
            $bairro = addslashes($_POST['bairro']);
            $rua = addslashes($_POST['rua']);
            $numero = addslashes($_POST['numero']);
            $complemento = addslashes($_POST['complemento']);

            if($verificaEmail = $u->verificaSeExiste($email)){
                if($u->createUser($email, $senha, $nome, $sobrenome, $telefone, $cpf, $cep, $estado, $cidade, $bairro, $rua, $numero, $complemento)){
                    header("Location: ".BASE_URL."users");
                    ?>
                
                <?php }
            }

        }

        $dados['user'] = $u->getDados();        

        $this->loadTemplate('register', $dados);
    }
}