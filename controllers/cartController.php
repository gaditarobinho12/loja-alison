<?php
class cartController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
      
    }

    public function index() {
        $store = new Store();
        $products = new Products();
        $cart = new Cart();
        $cep = '';
        $shipping = array();
        $u = new Users();
        $categories = new Categories();

        //print_r($_SESSION['cart']);


        if(!empty($_POST['cep'])) {
            $cep = intval($_POST['cep']);

            $shipping = $cart->shippingCalculate($cep);
            $_SESSION['shipping'] = $shipping;
        }

        if(!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];
        }

        

        $dados = $store->getTemplateData();

        $dados['verificaCode'] = 0;
        $dados['desconto'] = 0;
        $_SESSION['desconto'] = 0;


        if(isset($_POST['box_coupon_code']) && !empty($_POST['box_coupon_code'])){
            $code = addslashes($_POST['box_coupon_code']);

            $verificaCode = $cart->verificaCode($code);
            $dados['desconto'] = $verificaCode['coupon_value'];

            $_SESSION['desconto'] = $dados['desconto'];
        }

        if(isset($verificaCode)){
            $dados['verificaCode'] = $verificaCode;
        }

        $dados['categoriesMenu'] = $categories->getCatFromMenu();
        
        $dados['category'] = '';

        $dados['shipping'] = $shipping;
        $dados['list'] = $cart->getList();

        $dados['user'] = $u->getDados();
       
       
        $this->loadTemplate('cart', $dados);
    }

    public function del($id,$option) {
        if(!empty($id)) {
            unset($_SESSION['cart'][$id][$option]);

            if(!empty($_POST['cep'])) {
                $cep = intval($_POST['cep']);

                $shipping = $cart->shippingCalculate($cep);
                $_SESSION['shipping'] = $shipping;
            }

            if(!empty($_SESSION['shipping'])) {
                $shipping = $_SESSION['shipping'];
            }

        }

        header("Location: ".BASE_URL."cart");
        exit;
    }

    public function add() {

        if(!empty($_POST['id_product'])) {
            $id = intval($_POST['id_product']);
            $qt = intval($_POST['qt_product']);
            if(!isset($_SESSION['cart'][$id])){
            	$_SESSION['cart'][$id] = [];
            }

            if(!isset($_SESSION['cart'])) {
                $_SESSION['cart'] = array();
            }
            $size_qt = 1;
            if(isset($_SESSION['cart'][$id][$_POST['qt_option_size_id']]['quant'])){
            	$size_qt = $_SESSION['cart'][$id][$_POST['qt_option_size_id']]['quant']+1;
            }
           $c_qt = $this->checkQuant($id,$_POST['qt_option_size_id']);
           if($c_qt < $size_qt){
           	header("Location: ".BASE_URL."cart?error_qt=$c_qt");
           	exit;
           }


            if(isset($_SESSION['cart'][$id][$_POST['qt_option_size_id']])) {
                $_SESSION['cart'][$id][$_POST['qt_option_size_id']] = [
            		'option_id'=>$_POST['qt_option_size_id'],
            		'quant'=> $_SESSION['cart'][$id][$_POST['qt_option_size_id']]['quant']+1,
            		'id_product'=>$id
            	];
			
            } else {
         
                 $_SESSION['cart'][$id][$_POST['qt_option_size_id']] = [
            		'option_id'=>$_POST['qt_option_size_id'],
            		'quant'=>1,
            		'id_product'=>$id
            	];
            }
        }

        if(!empty($_POST['cep'])) {
            $cep = intval($_POST['cep']);

            $shipping = $cart->shippingCalculate($cep);
            $_SESSION['shipping'] = $shipping;
        }

        if(!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];
        }
        
        header("Location: ".BASE_URL."cart");
        exit;

    }
    
   
	function checkQuant($id,$product){
		$products = new Products();
		$size = $products->getOptionTypesSize($id,$product);
		if(count($size ) > 0){
		
			return $size['size']['quant'];
		}
		
		return 0;
	}

     public function increment($id,$option){
		$size_qt = 1;
            if(isset($_SESSION['cart'][$id][$option]['quant'])){
            	$size_qt = $_SESSION['cart'][$id][$option]['quant']+1;
            }
           $c_qt = $this->checkQuant($id,$option);
           if($c_qt < $size_qt){
           	header("Location: ".BASE_URL."cart?error_qt=$c_qt");
           	exit;
           }

     	$quant = [
     		'quant'=>	 $_SESSION['cart'][$id][$option]['quant']+1
     	];
     	
       
        $_SESSION['cart'][$id][$option] = array_merge($_SESSION['cart'][$id][$option],$quant);
        header("Location: ".BASE_URL."cart");
    }

    public function decrement($id,$option){
  
        $quant = [
     		'quant'=>	 $_SESSION['cart'][$id][$option]['quant']-1
     	];
     	
       
        $_SESSION['cart'][$id][$option] = array_merge($_SESSION['cart'][$id][$option],$quant);

        if(!empty($_POST['cep'])) {
            $cep = intval($_POST['cep']);

            $shipping = $cart->shippingCalculate($cep);
            $_SESSION['shipping'] = $shipping;
        }

        if(!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];
        }

        header("Location: ".BASE_URL."cart");
    }

    public function clear(){
        unset($_SESSION['cart']);
        header("Location: ".BASE_URL."cart");
    }

    public function payment_redirect(){
        if(!empty($_POST['payment_type'])){

            $payment_type = $_POST['payment_type'];

            switch($payment_type){
                case 'checkout_transparente':
                    header("Location: ".BASE_URL."psckttransparente");
                    exit;
                break;
                case 'boleto':
                    header("Location: ".BASE_URL."boleto");
                    exit;
                break;
            }

        } 
        header("Location: ".BASE_URL."cart");
        exit;
    }
}
