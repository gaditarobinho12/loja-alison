<?php
class usersController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])){
            $store = new Store();
            $products = new Products();
            $categories = new Categories();
            $f = new Filters();
            $u = new Users();
            $wishList = new WishList();

            $dados = $store->getTemplateData();

            $filters = array();
            if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
                $filters = $_GET['filter'];
            }

            $dados['categoriesMenu'] = $categories->getCatFromMenu();

            $dados['filters'] = $f->getFilters($filters);
            $dados['filters_selected'] = $filters;

            $dados['searchTerm'] = '';
            $dados['category'] = '';
            $dados['pedidos'] = $u->getPedidos();

            $dados['wishList'] = $wishList->getList();
            

            //$dados['sidebar'] = true;

            if(isset($_POST['email']) && !empty($_POST['email'])){
                $email = addslashes($_POST['email']);
                $senha = $_POST['senha'];

                $login = $u->logar($email, $senha);
            }

            if(isset($_POST['emailU']) && !empty($_POST['emailU'])){
                $senha = $_POST['password'];
                $name = addslashes($_POST['firstname']);
                $nick = addslashes($_POST['lastname']);
                $telefone = addslashes($_POST['phone']);

                $update = $u->update($senha, $name, $nick, $telefone);
            }

            $dados['user'] = $u->getDados();

            $this->loadTemplate('users', $dados);
        } else{
            header("Location: ".BASE_URL);
        }
    }

    public function updateAddress(){
        if(isset($_SESSION['usersLV']) && !empty($_SESSION['usersLV'])){
            $store = new Store();
            $products = new Products();
            $categories = new Categories();
            $f = new Filters();
            $u = new Users();

            $dados = $store->getTemplateData();

            $filters = array();
            if(!empty($_GET['filter']) && is_array($_GET['filter'])) {
                $filters = $_GET['filter'];
            }

            $dados['categoriesMenu'] = $categories->getCatFromMenu();

            $dados['filters'] = $f->getFilters($filters);
            $dados['filters_selected'] = $filters;

            $dados['searchTerm'] = '';
            $dados['category'] = '';
            $dados['pedidos'] = $u->getPedidos();
            

            //$dados['sidebar'] = true;

            if(isset($_POST['nome_entrega']) && !empty($_POST['nome_entrega'])){
                $nome = addslashes($_POST['nome_entrega']);
                $sobrenome = addslashes($_POST['sobrenome']);
                $rua = addslashes($_POST['rua_entrega']);
                $numero = addslashes($_POST['numero_entrega']);
                $complemento = addslashes($_POST['complemento_entrega']);
                $cidade = addslashes($_POST['cidade_entrega']);
                $bairro = addslashes($_POST['bairro_entrega']);
                $estado = addslashes($_POST['estado_entrega']);
                $telefone = addslashes($_POST['phone']);

                $updateAddress = $u->updateAddress($nome, $sobrenome, $rua, $numero, $complemento, $cidade, $bairro, $estado, $telefone);
            }

            $dados['user'] = $u->getDados();

            $this->loadTemplate('updateAddress', $dados);
        } else{
            header("Location: ".BASE_URL);
        }
    }

    public function sair(){
        unset($_SESSION['usersLV']);

        header("Location: ".BASE_URL);
    }
}