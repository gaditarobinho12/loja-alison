<?php
class wishListController extends controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function add($id){
    	$wishList = new wishList();

    	$add = $wishList->addWishlist($id, $_SESSION['usersLV']);
    }

    public function del($id){
    	$wishList = new wishList();

    	$del = $wishList->delWishlist($id);
    }

}