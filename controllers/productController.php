<?php
class productController extends Controller {

	private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        header("Location: ".BASE_URL);
    }


    public function open($id) {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $u = new Users();

        $dados = $store->getTemplateData();

        $info = $products->getProductInfo($id);

        if(count($info) > 0) {

            $dados['categoriesMenu'] = $categories->getCatFromMenu();

            $dados['product_info'] = $info;
            $dados['product_images'] = $products->getImagesByProductId($id);
            $dados['product_options'] = $products->getOptionsByProductId($id);

            if(isset($_POST['depoimento']) && !empty($_POST['depoimento'])){
                $depoimento = addslashes($_POST['depoimento']);
                $stars = addslashes($_POST['stars']);

                $addDepoimento = $products->addDepoimento($depoimento, $dados['product_info']['id'], $stars);
            }

            $dados['product_rates'] = $products->getRates($id, 5);
            
            $dados['category'] = '';

            $dados['user'] = $u->getDados();

            $this->loadTemplate('product', $dados);
        } else {
            header("Location: ".BASE_URL);
        }


    }
}